package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.ShippingChargeRequest;
import com.hcmute.fneststorebe.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/shipping-charge")
public class ShippingChargeController {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseBaseAbstract getShippingCharge(@RequestBody ShippingChargeRequest request) {
        return this.orderService.shippingCharge(request);
    }
}
