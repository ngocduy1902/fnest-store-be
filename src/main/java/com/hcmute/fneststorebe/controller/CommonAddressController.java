package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.repository.ProvinceCityRepository;
import com.hcmute.fneststorebe.service.interfaces.CommonAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/address")
public class CommonAddressController {
    @Autowired
    private CommonAddressService commonAddressService;

    @GetMapping("/province-city")
    public ResponseBaseAbstract getAllProvinceCity() {
        return this.commonAddressService.getAllProvinceCity();
    }

    @GetMapping("/district")
    public ResponseBaseAbstract getDistrictByProvinceCityName(@RequestParam String name) {
        return this.commonAddressService.getDistrictByProvinceCityName(name);
    }

    @GetMapping("/commune-ward-town")
    public ResponseBaseAbstract getCommuneWardTownByDistrictName(@RequestParam String name) {
        return this.commonAddressService.getCommuneWardTownByDistrictName(name);
    }
}
