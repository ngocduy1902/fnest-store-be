package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateFeedbackRequest;
import com.hcmute.fneststorebe.service.interfaces.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/feedback")
public class CommonFeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("")
    public ResponseBaseAbstract getAllFeedBack() {
        return this.feedbackService.getAllFeedback();
    }

    @PostMapping("")
    public ResponseBaseAbstract createFeedBack(@RequestBody CreateFeedbackRequest request) {
        return this.feedbackService.createFeedback(request);
    }
}
