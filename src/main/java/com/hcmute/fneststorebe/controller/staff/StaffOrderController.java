package com.hcmute.fneststorebe.controller.staff;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.UpdateOrderStatusRequest;
import com.hcmute.fneststorebe.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/staff/order")
public class StaffOrderController {
    @Autowired
    private OrderService orderService;

    @PatchMapping("/{id}")
    public ResponseBaseAbstract updateOrderStatus(@PathVariable Integer id,
                                                  @RequestBody UpdateOrderStatusRequest request) {
        request.setId(id);
        return this.orderService.updateOrderStatus(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllOrder() {
        return this.orderService.getAllOrder();
    }
}
