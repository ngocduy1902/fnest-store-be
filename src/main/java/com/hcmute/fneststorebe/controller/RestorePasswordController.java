package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.OtpConfirmRequest;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("api/restore-password")
public class RestorePasswordController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public ResponseBaseAbstract restorePassword(OtpConfirmRequest request) {
        return this.userService.restorePassword(request);
    }
}
