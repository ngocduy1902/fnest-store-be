package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/feedback")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("")
    public ResponseBaseAbstract getAllFeedback() {
        return this.feedbackService.getAllFeedback();
    }
}
