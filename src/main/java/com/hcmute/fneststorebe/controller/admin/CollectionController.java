package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCollectionRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCollectionRequest;
import com.hcmute.fneststorebe.service.interfaces.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/collection")
public class CollectionController {
    @Autowired
    private CollectionService collectionService;

    @PostMapping("")
    public ResponseBaseAbstract createCollection(@ModelAttribute CreateCollectionRequest request) throws IOException {
        return this.collectionService.createCollection(request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateCollection(@PathVariable Integer id,
                                                 @ModelAttribute UpdateCollectionRequest request) throws IOException {
        request.setId(id);
        return this.collectionService.updateCollection(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteCollection(@PathVariable Integer id) throws IOException {
        return this.collectionService.deleteCollection(id);
    }

}
