package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateUserRequest;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseBaseAbstract getAllUser() {
        return this.userService.getAllUser();
    }

    @GetMapping("/search")
    public ResponseBaseAbstract searchUser(@RequestParam Map<String, String> queries)
    {
        return this.userService.searchUser(queries);
    }

    @PostMapping("")
    public ResponseBaseAbstract createUser(@RequestBody CreateUserRequest request) {
        return this.userService.createUser(request);
    }

    @PatchMapping("/status/{id}")
    public ResponseBaseAbstract updateUserStatus(@PathVariable Integer id) {
        return this.userService.updateUserStatus(id);
    }
}
