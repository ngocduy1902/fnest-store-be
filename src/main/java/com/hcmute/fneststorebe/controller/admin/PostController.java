package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreatePostRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdatePostRequest;
import com.hcmute.fneststorebe.service.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/post")
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping("")
    public ResponseBaseAbstract getAllPost() {
        return this.postService.getAllPost();
    }

    @PostMapping("")
    public ResponseBaseAbstract createPost(@ModelAttribute CreatePostRequest request) throws IOException {
        return this.postService.createPost(request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updatePost(@PathVariable Integer id,
                                           @ModelAttribute UpdatePostRequest request) throws IOException {
        request.setId(id);
        return this.postService.updatePost(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deletePost(@PathVariable Integer id) throws IOException {
        return this.postService.deletePost(id);
    }
}
