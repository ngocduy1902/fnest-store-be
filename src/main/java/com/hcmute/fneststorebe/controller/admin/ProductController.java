package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateProductRequest;
import com.hcmute.fneststorebe.service.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("")
    public ResponseBaseAbstract createProduct(@RequestPart("info") CreateProductRequest request,
                                              @RequestPart("thumbnail") MultipartFile thumbnail,
                                              @RequestPart("images") List<MultipartFile> images) throws IOException {
        return this.productService.createProduct(request, thumbnail, images);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteProduct(@PathVariable Integer id) throws IOException {
        return this.productService.deleteProduct(id);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateProduct(@PathVariable Integer id,
                                              @RequestPart(value = "info") UpdateProductRequest request,
                                              @RequestPart(value = "thumbnail", required = false) MultipartFile thumbnail,
                                              @RequestPart(value = "images", required = false) List<MultipartFile> images) throws IOException {
        request.setId(id);
        return this.productService.updateProduct(request, thumbnail, images);
    }
}
