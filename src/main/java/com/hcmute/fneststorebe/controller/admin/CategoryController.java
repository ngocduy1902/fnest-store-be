package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCategoryRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCategoryRequest;
import com.hcmute.fneststorebe.service.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping("")
    public ResponseBaseAbstract createCategory(@ModelAttribute CreateCategoryRequest request) throws IOException {
        return this.categoryService.createCategory(request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateCategory(@PathVariable Integer id,
                                               @ModelAttribute UpdateCategoryRequest request) throws IOException {
        request.setId(id);
        return this.categoryService.updateCategory(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteCategory(@PathVariable Integer id) throws IOException {
        return this.categoryService.deleteCategory(id);
    }
}
