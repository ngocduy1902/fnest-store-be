package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateRoomRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateRoomRequest;
import com.hcmute.fneststorebe.service.interfaces.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/room")
public class RoomController {
    @Autowired
    private RoomService roomService;

    @PostMapping("")
    public ResponseBaseAbstract createRoom(@ModelAttribute CreateRoomRequest request) throws IOException {
        return this.roomService.createRoom(request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateRoom(@PathVariable Integer id,
                                           @ModelAttribute UpdateRoomRequest request) throws IOException {
        request.setId(id);
        return this.roomService.updateRoom(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteRoom(@PathVariable Integer id) throws IOException {
        return this.roomService.deleteRoom(id);
    }
}
