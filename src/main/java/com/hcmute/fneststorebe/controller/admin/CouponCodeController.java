package com.hcmute.fneststorebe.controller.admin;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCouponCodeRequest;
import com.hcmute.fneststorebe.service.interfaces.CouponCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/admin/coupon-code")
public class CouponCodeController {
    @Autowired
    private CouponCodeService couponCodeService;

    @PostMapping("")
    public ResponseBaseAbstract createCouponCode(@RequestBody CreateCouponCodeRequest request) {
        return this.couponCodeService.createCouponCode(request);
    }

    @PatchMapping("/{id}")
    public ResponseBaseAbstract updateCouponCode(@PathVariable Integer id,
                                                 @RequestBody UpdateCouponCodeRequest request) {
        request.setId(id);
        return this.couponCodeService.updateCouponCode(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteCouponCode(@PathVariable Integer id) {
        return this.couponCodeService.deleteCouponCode(id);
    }
}
