package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.EmailOtpRequest;
import com.hcmute.fneststorebe.service.OtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/otp")
public class OtpController {
    @Autowired
    private OtpService otpService;

    @PostMapping("")
    public ResponseBaseAbstract sendOtpViaEmail(@RequestBody EmailOtpRequest request) {
        return otpService.sendOtpViaEmail(request.getEmail());
    }
}
