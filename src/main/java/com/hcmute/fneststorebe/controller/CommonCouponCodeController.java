package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.ApplyCouponCodeRequest;
import com.hcmute.fneststorebe.service.interfaces.CouponCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/coupon-code")
public class CommonCouponCodeController {
    @Autowired
    private CouponCodeService couponCodeService;

    @PostMapping("/apply")
    public ResponseBaseAbstract applyCouponCode(ApplyCouponCodeRequest request) {
        return this.couponCodeService.applyCouponCode(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllCouponCode() {
        return this.couponCodeService.getAllCoupon();
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getCouponCodeById(@PathVariable Integer id) {
        return this.couponCodeService.getCouponCodeById(id);
    }
}
