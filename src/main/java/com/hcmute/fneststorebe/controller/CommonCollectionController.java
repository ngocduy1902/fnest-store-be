package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/collection")
public class CommonCollectionController {
    @Autowired
    private CollectionService collectionService;

    @GetMapping("")
    public ResponseBaseAbstract getAllCollection() {
        return this.collectionService.getAllCollection();
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getCollectionById(@PathVariable Integer id) {
        return this.collectionService.getCollectionById(id);
    }
}
