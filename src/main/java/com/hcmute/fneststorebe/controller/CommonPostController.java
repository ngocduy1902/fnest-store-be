package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("api/post")
public class CommonPostController {
    @Autowired
    private PostService postService;

    @GetMapping("")
    public ResponseBaseAbstract getPostPage(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                               @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.postService.getPostPage(currentPage, pageSize);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getPostById(@PathVariable Integer id) {
        return this.postService.getPostDetail(id);
    }

    @GetMapping("search")
    public ResponseBaseAbstract searchPost(@RequestParam Map<String, String> queries) {
        return this.postService.searchPost(queries);
    }

    @GetMapping("banner/name")
    public ResponseBaseAbstract searchPostByBanner(@RequestParam String name) {
        return this.postService.getPostByBannerName(name);
    }
}
