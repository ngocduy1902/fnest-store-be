package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/category")
public class CommonCategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("")
    public ResponseBaseAbstract getAllCategory() {
        return this.categoryService.getAllCategory();
    }

    @GetMapping("/by-room")
    public ResponseBaseAbstract getAllCategoryByRoomId(@RequestParam Integer id) {
        return this.categoryService.getCategoryByRoomId(id);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getCategoryById(@PathVariable Integer id) {
        return this.categoryService.getCategoryById(id);
    }
}
