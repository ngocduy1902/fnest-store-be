package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.ProductReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/product-review")
public class CommonProductReviewController {
    @Autowired
    private ProductReviewService productReviewService;

    @GetMapping("/by-product")
    public ResponseBaseAbstract getAllProductReviewByProduct(@RequestParam String productName,
                                                             @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                             @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.productReviewService.getAllReviewByProduct(productName, currentPage, pageSize);
    }
}
