package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("api/product")
public class CommonProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/{id}")
    public ResponseBaseAbstract getProductById(@PathVariable Integer id) {
        return this.productService.getProductById(id);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllProduct() {
        return this.productService.getAllProduct();
    }

    @GetMapping("/by-category")
    public ResponseBaseAbstract getProductByCategoryName(@RequestParam String categoryName,
                                                         @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                         @RequestParam(required = false, defaultValue = "12") Integer pageSize,
                                                         @RequestParam(required = false) String sort) {
        return this.productService.getProductByCategoryName(categoryName, currentPage, pageSize, sort);
    }

    @GetMapping("/search-filter")
    public ResponseBaseAbstract searchAndFilterProduct(@RequestParam Map<String, String> queries) {
        return this.productService.searchAndFilterProduct(queries);
    }

    @GetMapping("/by-collection")
    public ResponseBaseAbstract getProductByCollectionId(@RequestParam Integer id,
                                                         @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                         @RequestParam(required = false, defaultValue = "12") Integer pageSize,
                                                         @RequestParam(required = false) String sort)
    {
        return this.productService.getProductByCollectionId(id, currentPage, pageSize, sort);
    }

    @GetMapping("/featured-page")
    public ResponseBaseAbstract getAllFeaturedProductPage(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                          @RequestParam(required = false, defaultValue = "12") Integer pageSize,
                                                          @RequestParam(required = false) String sort) {
        return this.productService.getFeaturedProductPage(currentPage, pageSize, sort);
    }
}
