package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.LoginRequest;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/auth/login")
public class LoginController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public ResponseBaseAbstract login(@RequestBody LoginRequest request) {
        return this.userService.authenticate(request);
    }
}
