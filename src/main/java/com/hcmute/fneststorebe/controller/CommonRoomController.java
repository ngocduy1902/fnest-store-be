package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.service.interfaces.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/room")
public class CommonRoomController {
    @Autowired
    private RoomService roomService;

    @GetMapping("")
    public ResponseBaseAbstract getAllRoom() {
        return this.roomService.getAllRoom();
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getRoomById(@PathVariable Integer id) {
        return this.roomService.getRoomById(id);
    }
}
