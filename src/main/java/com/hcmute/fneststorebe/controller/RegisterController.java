package com.hcmute.fneststorebe.controller;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.UserRegisterRequest;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/auth/register")
public class RegisterController {
    @Autowired
    private UserService userService;

    @PostMapping("")
    public ResponseBaseAbstract registerUser(@RequestBody UserRegisterRequest request) {
        ResponseBaseAbstract response = this.userService.registerUser(request);
        return response;
    }

    @PostMapping("/admin")
    public ResponseBaseAbstract registerAdmin() {
        return this.userService.registerAdminAccount();
    }
}
