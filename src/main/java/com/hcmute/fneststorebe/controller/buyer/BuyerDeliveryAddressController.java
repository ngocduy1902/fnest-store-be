package com.hcmute.fneststorebe.controller.buyer;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.fneststorebe.domain.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.service.interfaces.DeliveryAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/buyer/delivery-address")
public class BuyerDeliveryAddressController {
    @Autowired
    private DeliveryAddressService deliveryAddressService;

    @GetMapping("")
    public ResponseBaseAbstract getAllDeliveryAdress(@AuthenticationPrincipal User user) {
        return this.deliveryAddressService.getAllDeliveryAddressByUser(user);
    }

    @PostMapping("")
    public ResponseBaseAbstract createDeliveryAddress(@AuthenticationPrincipal User user,
                                                      @RequestBody CreateDeliveryAddressRequest request) {
        return this.deliveryAddressService.createDeliveryAddress(user, request);
    }

    @PatchMapping("/default")
    public ResponseBaseAbstract updateDefaultAddress(@AuthenticationPrincipal User user,
                                                     @RequestBody UpdateDefaultAddressRequest request) {
        return this.deliveryAddressService.updateDefaultAddress(user, request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateDeliveryAddress(@PathVariable Integer id,
                                                      @AuthenticationPrincipal User user,
                                                      @RequestBody UpdateDeliveryAddressRequest request) {
        request.setId(id);
        return this.deliveryAddressService.updateDeliveryAddress(user, request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteDeliveryAddress(@AuthenticationPrincipal User user,
                                                      @PathVariable Integer id) {
        return this.deliveryAddressService.deleteDeliveryAddress(user, id);
    }
}
