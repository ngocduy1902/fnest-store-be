package com.hcmute.fneststorebe.controller.buyer;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateOrderRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateOrderStatusRequest;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/buyer/order")
public class BuyerOrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("")
    public ResponseBaseAbstract getOrderByUser(@AuthenticationPrincipal User user,
                                               @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                               @RequestParam(required = false, defaultValue = "12") Integer pageSize)
    {
        return this.orderService.getOrderByUser(user, currentPage, pageSize);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getOrderDetail(@PathVariable Integer id) {
        return this.orderService.getOrderDetail(id);
    }

    @PostMapping("")
    public ResponseBaseAbstract createOrder(@AuthenticationPrincipal User user,
                                            @RequestBody CreateOrderRequest request) {
        return this.orderService.createOrder(user, request);
    }

    @PatchMapping("/{id}")
    public ResponseBaseAbstract cancelOrder(@PathVariable Integer id,
                                            @RequestBody UpdateOrderStatusRequest request) {
        request.setId(id);
        return this.orderService.cancelOrder(request);
    }

    @GetMapping("/by-status")
    public ResponseBaseAbstract getOrderByStatus(@RequestParam OrderStatus status,
                                                 @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                 @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.orderService.getOrderByStatus(status, currentPage, pageSize);
    }
}
