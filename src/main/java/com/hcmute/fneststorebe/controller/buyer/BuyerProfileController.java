package com.hcmute.fneststorebe.controller.buyer;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.EmailConfirmationRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdatePasswordRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateProfileRequest;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/buyer/profile")
public class BuyerProfileController {
    @Autowired
    private UserService userService;

    @PostMapping("/email-confirm")
    public ResponseBaseAbstract emailConfirmation(@AuthenticationPrincipal User user,
                                                  @RequestBody EmailConfirmationRequest request)
    {
        return this.userService.confirmClientEmail(user, request);
    }

    @PatchMapping("")
    public ResponseBaseAbstract updateProfile(@AuthenticationPrincipal User user,
                                              @RequestBody UpdateProfileRequest request) {
        return this.userService.updateProfile(user, request);
    }

    @PatchMapping("/password")
    public ResponseBaseAbstract updatePassword(@AuthenticationPrincipal User user,
                                               @RequestBody UpdatePasswordRequest request) {
        return this.userService.updatePassword(user, request);
    }
}
