package com.hcmute.fneststorebe.controller.buyer;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductReviewRequest;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.service.interfaces.ProductReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/buyer/product-review")
public class BuyerProductReviewController {
    @Autowired
    private ProductReviewService productReviewService;

    @PostMapping("")
    public ResponseBaseAbstract createReview(@AuthenticationPrincipal User user,
                                             @RequestBody CreateProductReviewRequest request) {
        return this.productReviewService.createReview(user, request);
    }
}
