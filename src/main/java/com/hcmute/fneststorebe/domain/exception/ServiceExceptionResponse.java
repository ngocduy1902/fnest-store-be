package com.hcmute.fneststorebe.domain.exception;


import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;

public class ServiceExceptionResponse extends ResponseBaseAbstract {
    public ServiceExceptionResponse() {
        super();
        this.setStatus("FAIL");
    }
}
