package com.hcmute.fneststorebe.domain.enums;

public enum UserGender {
    MALE,
    FEMALE,
    OTHER
}
