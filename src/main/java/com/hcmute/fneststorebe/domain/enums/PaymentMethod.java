package com.hcmute.fneststorebe.domain.enums;

public enum PaymentMethod {
    COD,
    Banking
}
