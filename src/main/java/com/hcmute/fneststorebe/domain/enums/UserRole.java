package com.hcmute.fneststorebe.domain.enums;

public enum UserRole {
    ADMIN,
    BUYER,
    STAFF
}
