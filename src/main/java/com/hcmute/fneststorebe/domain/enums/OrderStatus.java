package com.hcmute.fneststorebe.domain.enums;

public enum OrderStatus {
    PENDING,
    CONFIRMED,
    IN_SHIPPING,
    COMPLETED,
    CANCELED,
    REVIEWED
}
