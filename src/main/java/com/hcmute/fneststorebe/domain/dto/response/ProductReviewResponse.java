package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.ProductReview;
import lombok.Data;

import java.util.Date;

@Data
public class ProductReviewResponse {
    private Integer id;
    private String reviewerName;
    private Integer point;
    private String content;
    private Date createdAt;

    public ProductReviewResponse(ProductReview productReview) {
        this.id = productReview.getId();
        this.reviewerName = productReview.getUser().getFullName();
        this.point = productReview.getPoint();
        this.content = productReview.getContent();
        this.createdAt = productReview.getCreatedAt();
    }
}
