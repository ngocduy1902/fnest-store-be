package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Collection;
import lombok.Data;

@Data
public class CollectionResponse {
    private Integer id;
    private String name;
    private String description;
    private String imageUrl;

    public CollectionResponse(Collection collection) {
        this.id = collection.getId();
        this.name = collection.getName();
        this.description = collection.getDescription();
        this.imageUrl = collection.getImage();
    }
}
