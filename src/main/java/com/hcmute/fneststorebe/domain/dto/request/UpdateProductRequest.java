package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateProductRequest {
    @JsonIgnore
    private Integer id;
    private String name;
    private Double price;
    private Double salePrice;
    private String description;
    private String size;
    private String material;
    private Integer inStock;
    private boolean featured;
    private Integer categoryId;
    private Integer collectionId;
}
