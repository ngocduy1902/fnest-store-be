package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Category;
import lombok.Data;

@Data
public class CategoryResponse {
    private Integer id;
    private String name;
    private String image;
    private Integer roomId;

    public CategoryResponse(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.image = category.getImage();
        this.roomId = category.getRoom() != null ? category.getRoom().getId() : null;
    }
}
