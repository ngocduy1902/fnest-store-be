package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class CreateDeliveryAddressRequest {
    private String addressDetail;
    private String receiverName;
    private String receiverPhone;
    private String CommuneWardTownName;
    private String DistrictName;
    private String ProvinceCityName;
}
