package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class EmailConfirmationRequest {
    private String otpCode;
}
