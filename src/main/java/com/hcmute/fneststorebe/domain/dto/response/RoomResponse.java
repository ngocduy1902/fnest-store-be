package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Room;
import lombok.Data;

@Data
public class RoomResponse {
    private Integer id;
    private String name;
    private String image;

    public RoomResponse(Room room) {
        this.id = room.getId();
        this.name = room.getName();
        this.image = room.getImage();
    }
}
