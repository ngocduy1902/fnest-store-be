package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Order;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class OrderResponse {
    private Integer id;
    private OrderStatus status;
    private Double total;
    List<OrderItemResponse> responses = new ArrayList<>();

    public OrderResponse(Order order) {
        this.id = order.getId();
        this.status = order.getStatus();
        this.total = order.getTotal();
    }
}
