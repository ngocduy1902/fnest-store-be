package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateRoomRequest {
    private String name;
    private MultipartFile image;
}
