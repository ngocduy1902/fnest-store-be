package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import lombok.Data;

@Data
public class UpdateOrderStatusRequest {
    @JsonIgnore
    private Integer id;
    private OrderStatus status;
}
