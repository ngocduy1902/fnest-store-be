package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateCollectionRequest {
    private String name;
    private String description;
    private MultipartFile image;
}
