package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

@Data
public class UpdateCouponCodeRequest {
    @JsonIgnore
    private Integer id;
    private String code;
    private Integer value;
    private String description;
    private Double minOrderValue;
    private Double maxDiscount;
    private Integer times;
    private Date beginDate;
    private Date endDate;
}
