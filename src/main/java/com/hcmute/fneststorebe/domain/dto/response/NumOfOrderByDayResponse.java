package com.hcmute.fneststorebe.domain.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class NumOfOrderByDayResponse {
    private Date date;
    private Integer amount;
}
