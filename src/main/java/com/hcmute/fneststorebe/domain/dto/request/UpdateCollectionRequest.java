package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateCollectionRequest {
    @JsonIgnore
    private Integer id;
    private String name;
    private String description;
    private MultipartFile image;
}
