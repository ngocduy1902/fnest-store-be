package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.CouponCode;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Date;

@Data
public class CouponCodeResponse {
    private Integer id;
    private String code;
    private Integer value;
    private String description;
    private Double minOrderValue;
    private Double maxDiscount;
    private Integer times;
    private Date beginDate;
    private Date endDate;

    public CouponCodeResponse(CouponCode couponCode) {
        this.id = couponCode.getId();
        this.code = couponCode.getCode();
        this.value = couponCode.getValue();
        this.description = couponCode.getDescription();
        this.minOrderValue = couponCode.getMinOrderValue();
        this.maxDiscount = couponCode.getMaxDiscount();
        this.times = couponCode.getTimes();
        this.beginDate = couponCode.getBeginDate();
        this.endDate = couponCode.getEndDate();
    }
}
