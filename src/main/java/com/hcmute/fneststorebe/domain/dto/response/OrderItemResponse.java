package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.OrderItem;
import lombok.Data;

@Data
public class OrderItemResponse {
    private Integer id;
    private String productName;
    private String productSize;
    private String productMaterial;
    private String productThumbnail;
    private Double productPrice;
    private Integer quantity;
    private Double total;

    public OrderItemResponse(OrderItem item) {
        this.id = item.getId();
        this.productName = item.getProductName();
        this.productSize = item.getProductSize();
        this.productMaterial = item.getProductMaterial();
        this.productThumbnail = item.getProductThumbnail();
        this.productPrice = item.getProductPrice();
        this.quantity = item.getQuantity();
        this.total = item.getTotal();
    }
}
