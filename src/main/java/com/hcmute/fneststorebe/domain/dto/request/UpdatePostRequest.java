package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class UpdatePostRequest {
    @JsonIgnore
    private Integer id;
    private String title;
    private String description;
    private List<MultipartFile> banners;
}
