package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class ShippingChargeRequest {
    private Double total;
}
