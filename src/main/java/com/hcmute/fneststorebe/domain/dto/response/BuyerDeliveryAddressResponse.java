package com.hcmute.fneststorebe.domain.dto.response;

import lombok.Data;

@Data
public class BuyerDeliveryAddressResponse {
    private Integer id;
    private String deliveryAddress;
    private String receiverName;
    private String receiverPhone;

    public BuyerDeliveryAddressResponse(Integer id, String address, String receiverName, String receiverPhone) {
        this.id = id;
        this.deliveryAddress = address;
        this.receiverName = receiverName;
        this.receiverPhone = receiverPhone;
    }
}
