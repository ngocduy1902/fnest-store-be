package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateCategoryRequest {
    @JsonIgnore
    private Integer id;
    private String name;
    private MultipartFile image;
    private Integer roomId;
}
