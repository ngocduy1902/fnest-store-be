package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class ApplyCouponCodeRequest {
    private Double orderTotal;
    private String code;
}
