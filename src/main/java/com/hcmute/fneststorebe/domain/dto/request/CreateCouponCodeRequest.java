package com.hcmute.fneststorebe.domain.dto.request;

import jakarta.persistence.Column;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Date;

@Data
public class CreateCouponCodeRequest {
    private String code;
    private Integer value;
    private String description;
    private Double minOrderValue;
    private Double maxDiscount;
    private Integer times;
    private Date beginDate;
    private Date endDate;
}
