package com.hcmute.fneststorebe.domain.dto.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class OtpConfirmRequest {
    @NotEmpty
    private String username;

    @NotEmpty
    private String otpCode;
}
