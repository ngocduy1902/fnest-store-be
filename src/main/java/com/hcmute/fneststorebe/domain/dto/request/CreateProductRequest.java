package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class CreateProductRequest {
    private String name;
    private Double price;
    private Double salePrice;
    private String description;
    private String size;
    private String material;
    private Integer inStock;
    private boolean featured;
    private Integer categoryId;
    private Integer collectionId;
}
