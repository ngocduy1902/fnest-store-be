package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Product;
import lombok.Data;

import java.util.List;

@Data
public class ProductResponse {
    private Integer id;
    private String name;
    private boolean onSale;
    private Double price;
    private Double salePrice;
    private String description;
    private String size;
    private String material;
    private Integer sold;
    private Integer inStock;
    private boolean featured;
    private Integer categoryId;
    private Integer collectionId;
    private String thumbnail;
    private Integer reviewAmount;
    private Integer totalReviewPoint;
    private List<String> imageUrls;

    public ProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.onSale = product.isOnSale();
        this.price = product.getPrice();
        this.salePrice = product.getSalePrice() != null ? product.getSalePrice() : null;
        this.description = product.getDescription();
        this.size = product.getSize();
        this.material = product.getMaterial();
        this.sold = product.getSold();
        this.inStock = product.getInStock();
        this.featured = product.isFeatured();
        this.categoryId = product.getCategory() != null ? product.getCategory().getId() : null;
        this.collectionId = product.getCollection() != null ? product.getCollection().getId() : null;
        this.thumbnail = product.getThumbnail();
        this.reviewAmount = product.getReviewAmmount();
        this.totalReviewPoint = product.getTotalReviewPoint();
    }
}
