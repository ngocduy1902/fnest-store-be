package com.hcmute.fneststorebe.domain.dto.request;

import com.hcmute.fneststorebe.domain.enums.UserGender;
import lombok.Data;

import java.util.Date;

@Data
public class UserRegisterRequest {
    private String username;
    private String password;
    private String fullName;
    private String email;
    private UserGender gender;
    private Date birthday;
}
