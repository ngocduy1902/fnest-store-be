package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class CreateOrderItemRequest {
    private Integer productId;
    private Integer quantity;
}
