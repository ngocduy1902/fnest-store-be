package com.hcmute.fneststorebe.domain.dto.response;

import lombok.Data;

@Data
public class SoldByProductResponse {
    private Integer productId;
    private Integer soldCount;

    public SoldByProductResponse(Integer productId, Integer soldCount) {
        this.productId = productId;
        this.soldCount = soldCount;
    }
}
