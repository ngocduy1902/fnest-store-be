package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.fneststorebe.domain.enums.UserGender;
import lombok.Data;

import java.util.Date;

@Data
public class UpdateProfileRequest {
    private String fullName;
    private String email;
    private UserGender gender;
    private Date birthday;
}
