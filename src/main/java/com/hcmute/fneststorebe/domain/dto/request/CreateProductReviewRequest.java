package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class CreateProductReviewRequest {
    private Integer orderItemId;
    private String content;
    private Integer point;
}
