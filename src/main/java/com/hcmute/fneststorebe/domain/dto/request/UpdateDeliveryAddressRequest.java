package com.hcmute.fneststorebe.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateDeliveryAddressRequest {
    @JsonIgnore
    private Integer id;
    private String addressDetail;
    private String receiverName;
    private String receiverPhone;
    private String CommuneWardTownName;
    private String DistrictName;
    private String ProvinceCityName;
}
