package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Post;
import lombok.Data;

import java.util.List;

@Data
public class PostResponse {
    private Integer id;
    private String title;
    private String description;
    private List<String> banners;

    public PostResponse(Post post) {
        this.id = post.getId();
        this.title = post.getTitle();
        this.description = post.getDescription();
        this.banners = post.getBanners();
    }
}
