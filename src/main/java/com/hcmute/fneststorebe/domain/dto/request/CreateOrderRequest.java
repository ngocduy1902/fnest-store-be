package com.hcmute.fneststorebe.domain.dto.request;

import com.hcmute.fneststorebe.domain.enums.PaymentMethod;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateOrderRequest {
    private Double shippingCharge;
    private Integer deliveryAddressId;
    private String code;
    private PaymentMethod paymentMethod;
    private List<CreateOrderItemRequest> orderItemList = new ArrayList<>();
}
