package com.hcmute.fneststorebe.domain.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class IncomeByDayResponse {
    private Date date;
    private Double incomeByDay;
}
