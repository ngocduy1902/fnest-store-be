package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.Order;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.domain.enums.PaymentMethod;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class OrderDetailResponse {
    private Integer id;
    private Date createdAt;
    private Date lastUpdatedAt;
    private OrderStatus status;
    private BuyerDeliveryAddressResponse deliveryAddress;
    private Double shippingCharge;
    private Double codeDiscount;
    private Double total;
    private PaymentMethod paymentMethod;
    List<OrderItemResponse> orderItemList = new ArrayList<>();

    public OrderDetailResponse(Order order) {
        this.id = order.getId();
        this.createdAt = order.getCreatedAt();
        this.lastUpdatedAt = order.getLastUpdatedAt();
        this.status = order.getStatus();
        this.shippingCharge = order.getShippingCharge().getCharge();
        this.total = order.getTotal();
        this.paymentMethod = order.getPaymentMethod();
    }
}
