package com.hcmute.fneststorebe.domain.dto.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class EmailOtpRequest {
    @NotEmpty
    private String email;
}
