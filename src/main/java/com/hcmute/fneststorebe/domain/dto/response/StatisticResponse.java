package com.hcmute.fneststorebe.domain.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class StatisticResponse {
    private Integer soldProduct;
    private Integer numOfUser;
    private Double income;
    private Integer numOfOrderByMonth;
    private ProductResponse productOfTheMonth;
    private List<SoldByProductResponse> soldByProductResponses;
    private List<NumOfOrderByDayResponse> numOfOrderByDays;
    private List<IncomeByDayResponse> incomeByDays;
}
