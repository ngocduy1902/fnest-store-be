package com.hcmute.fneststorebe.domain.dto.response;

import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.UserGender;
import com.hcmute.fneststorebe.domain.enums.UserRole;
import lombok.Data;

import java.util.Date;

@Data
public class UserResponse {
    private Integer id;
    private Date createdAt;
    private Date lastUpdatedAt;
    private Date deletedAt;
    private String username;
    private String email;
    private String fullName;
    private UserRole role;
    private UserGender gender;
    private Date birthday;
    private boolean emailConfirmed;
    private boolean status;

    public UserResponse(User user) {
        this.id = user.getId();
        this.createdAt = user.getCreatedAt();
        this.lastUpdatedAt = user.getLastUpdatedAt();
        this.deletedAt = user.getDeletedAt();
        this.username = user.getUsername();
        this.fullName = user.getFullName();
        this.role = user.getRole();
        this.email = user.getEmail();
        this.gender = user.getGender();
        this.birthday = user.getBirthday();
        this.emailConfirmed = user.isEmailConfirmed();
        this.status = user.isStatus();
    }
}
