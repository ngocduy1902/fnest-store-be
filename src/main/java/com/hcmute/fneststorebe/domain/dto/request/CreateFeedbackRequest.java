package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;

@Data
public class CreateFeedbackRequest {
    private String fullName;
    private String email;
    private String content;

}
