package com.hcmute.fneststorebe.domain.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class CreatePostRequest {
    private String title;
    private String description;
    private List<MultipartFile> banners;
}
