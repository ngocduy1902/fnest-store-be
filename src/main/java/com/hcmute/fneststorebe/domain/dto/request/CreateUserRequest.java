package com.hcmute.fneststorebe.domain.dto.request;

import com.hcmute.fneststorebe.domain.enums.UserRole;
import lombok.Data;

@Data
public class CreateUserRequest {
    private String username;

    private String password;

    private String displayName;

    private UserRole role;

}
