package com.hcmute.fneststorebe.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Data
@Table(name = "tbl_coupon_code")
public class CouponCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name = "code")
    private String code;

    @Column(name = "value") // percents discounted
    private Integer value;

    @Column(name = "description")
    private String description;

    @Column(name = "min_order_value")// >= minOrderValue can apply coupon
    private Double minOrderValue;

    @Column(name = "max_discount")// max value possible when discount
    private Double maxDiscount;

    @Column(name = "times")
    private Integer times;

    @Column(name = "begin_date")
    private Date beginDate;

    @Column(name = "end_date")
    private Date endDate;
}
