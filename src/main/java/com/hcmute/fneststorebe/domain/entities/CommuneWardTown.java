package com.hcmute.fneststorebe.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@Table(name = "tbl_commune_ward_town")
public class CommuneWardTown {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name = "name")
    private String name;

    @Column(name = "ghn_ward_code")
    private String ghnWardCode;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;
}
