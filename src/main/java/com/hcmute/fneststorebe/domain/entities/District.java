package com.hcmute.fneststorebe.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@Table(name = "tbl_district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name = "name")
    private String name;

    @Column(name = "ghn_district_id")
    private Integer ghnDistrictId;

    @ManyToOne
    @JoinColumn(name = "province_city_id")
    private ProvinceCity provinceCity;
}
