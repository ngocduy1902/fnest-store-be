package com.hcmute.fneststorebe.domain.entities;


import com.hcmute.fneststorebe.domain.enums.UserGender;
import com.hcmute.fneststorebe.domain.enums.UserRole;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_user")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "fullName", columnDefinition = "NVARCHAR(255)")
    private String fullName;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "status")
    private boolean status = true;

    @Enumerated(EnumType.STRING)
    private UserGender gender;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "email_confirmed")
    private boolean emailConfirmed;

    @OneToOne(cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "default_address_id", referencedColumnName = "id")
    private DeliveryAddress defaultAddress;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String roleName = role.name();
        if (roleName.equals(UserRole.ADMIN) || roleName.equals(UserRole.STAFF))
            authorities.add(new SimpleGrantedAuthority(roleName));
        else{
            if (!status)
                authorities.add(new SimpleGrantedAuthority("LOCKED_BUYER"));
            else
                authorities.add(new SimpleGrantedAuthority(roleName));
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return status;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status;
    }
}
