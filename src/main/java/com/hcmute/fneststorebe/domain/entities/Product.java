package com.hcmute.fneststorebe.domain.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="tbl_product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name="name")
    private String name;

    @Column(name="on_sale")
    private boolean onSale;

    @Column(name="price")
    private Double price;

    @Column(name="sale_price")
    private Double salePrice;

    @Column(name="description", columnDefinition = "TEXT")
    private String description;

    @Column(name="size")
    private String size;

    @Column(name = "material")
    private String material;

    @Column(name = "sold")
    private Integer sold;

    @Column(name = "in_stock")
    private Integer inStock;

    @Column(name = "is_featured")
    private boolean isFeatured;

    @Column(name = "total_review_point")
    private Integer totalReviewPoint;

    @Column(name = "review_ammount")
    private Integer reviewAmmount;

    @Column(name = "thumbnail")
    private String thumbnail;

    @ManyToOne
    @JoinColumn(name="category_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Category category;

    @ManyToOne
    @JoinColumn(name="collection_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection collection;
}
