package com.hcmute.fneststorebe.domain.entities;

import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.domain.enums.PaymentMethod;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Date;
import java.util.List;


@Entity
@Data
@Table(name="tbl_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createdAt")
    private Date createdAt = new Date();

    @Column(name = "lastUpdatedAt")
    private Date lastUpdatedAt = new Date();

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name="total")
    private Double total;

    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @OneToOne
    @JoinColumn(name = "shipping_charge_id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private ShippingCharge shippingCharge;

    @ManyToOne
    @JoinColumn(name="user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @ManyToOne
    @JoinColumn(name = "delivery_address_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private DeliveryAddress deliveryAddress;

    @OneToMany(mappedBy = "order", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<OrderItem> orderItems;

    @ManyToOne
    @JoinColumn(name = "coupon_code_id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private CouponCode couponCode;

    public void useCouponCode() {
        Double discount = this.total * this.couponCode.getValue();
        Double finalDiscount = discount >= this.couponCode.getMaxDiscount() ? this.couponCode.getMaxDiscount() : discount;
        this.total -= finalDiscount;
    }
}
