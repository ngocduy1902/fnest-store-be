package com.hcmute.fneststorebe.oauth;

import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.UserRole;
import com.hcmute.fneststorebe.jwt.JwtTokenProvider;
import com.hcmute.fneststorebe.repository.UserRepository;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Random;

@Component
public class OAuthAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	private final UserRepository userRepository;

	private static final Random rand = new Random();
	
	private final JwtTokenProvider jwtTokenProvider;
	
	@Value("${com.hcmute.fneststorebe.security.oauth2.fe-login-url}")
	String redirectUrl;
	
	public OAuthAuthenticationSuccessHandler(@Lazy UserRepository userRepository, @Lazy JwtTokenProvider jwtTokenProvider) {
		this.userRepository = userRepository;
		this.jwtTokenProvider = jwtTokenProvider;
	}
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
										Authentication authentication) throws IOException, ServletException {
		
		OAuth2User user = (OAuth2User)authentication.getPrincipal();
		//CustomOAuthUser user = (CustomOAuthUser)oAuth2User;
		String email = user.getAttribute("email");
		User userEntity = new User();
		String username;
		if (!this.userRepository.existsByEmail(email)) {
			username = generateUsername(email.substring(0, email.indexOf("@")));
			userEntity.setUsername(username);
			userEntity.setFullName(user.getAttribute("name"));
			userEntity.setEmail(email);
			userEntity.setRole(UserRole.BUYER);
			userEntity.setEmailConfirmed(true);

			this.userRepository.save(userEntity);
		}
		else
			userEntity = this.userRepository.getUserByEmail(email);
		
		getRedirectStrategy().sendRedirect(
				request, 
				response, 
				redirectUrl + "?token=" + this.jwtTokenProvider.generateToken(userEntity));
		
		super.onAuthenticationSuccess(request, response, authentication);
	}

	private String generateUsername(String username) {
		if (this.userRepository.existsByUsername(username)) {
			String usernameOrigin = username;
			do {
				username = usernameOrigin + this.rand.nextInt(3,9);
			} while (this.userRepository.existsByUsername(username));
		}
		return username;
	}
}
