package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCategoryRequest;
import com.hcmute.fneststorebe.domain.dto.response.CategoryResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateCategoryRequest;
import com.hcmute.fneststorebe.domain.entities.Category;
import com.hcmute.fneststorebe.domain.entities.Room;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.CategoryRepository;
import com.hcmute.fneststorebe.repository.RoomRepository;
import com.hcmute.fneststorebe.service.interfaces.CategoryService;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public ResponseBaseAbstract getCategoryByRoomId(Integer id) {
        if (this.roomRepository.getRoomById(id) == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy room với id = " + id);
        }
        List<Category> categoryList = this.categoryRepository.getCategoryByRoomId(id);
        List<CategoryResponse> data = categoryList.stream().map(category -> new CategoryResponse(category)).toList();
        for (CategoryResponse response : data) {
            response.setImage(this.firebaseService.getImageUrl(response.getImage()));
        }

        return SuccessResponse.builder()
                .addMessage("Lây dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createCategory(CreateCategoryRequest request) throws IOException {
        Category category = new Category();
        category.setName(request.getName());
        String image = this.firebaseService.save(request.getImage());
        category.setImage(image);
        if (request.getRoomId() != null) {
            Room room = this.roomRepository.getRoomById(request.getRoomId());
            if (room == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy phòng với id = " + request.getRoomId());
            category.setRoom(room);
        }
        this.categoryRepository.save(category);
        CategoryResponse data = new CategoryResponse(category);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllCategory() {
        List<Category> categoryList = this.categoryRepository.findAll();

        List<CategoryResponse> data = categoryList.stream().map(category -> new CategoryResponse(category)).toList();
        if (data.size() != 0) {
            for (CategoryResponse categoryResponse : data) {
                categoryResponse.setImage(this.firebaseService.getImageUrl(categoryResponse.getImage()));
            }
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getCategoryById(Integer id) {
        Category category = this.categoryRepository.getCategoryById(id);
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy category với id = "+ id);
        CategoryResponse data = new CategoryResponse(category);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateCategory(UpdateCategoryRequest request) throws IOException {
        Category category = this.categoryRepository.getCategoryById(request.getId());
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy category với id = "+ request.getId());
        category.setName(request.getName());
        if (request.getImage() != null) {
            this.firebaseService.delete(category.getImage());
            category.setImage(this.firebaseService.save(request.getImage()));
        }

        if (request.getRoomId() != null) {
            Room room = this.roomRepository.getRoomById(request.getRoomId());
            if (room == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy phòng với id = " + request.getRoomId());
            category.setRoom(room);
        }
        this.categoryRepository.save(category);

        CategoryResponse data = new CategoryResponse(category);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteCategory(Integer id) throws IOException {
        Category category = this.categoryRepository.getCategoryById(id);
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy category với id = "+ id);
        if (category.getImage() != null) {
            this.firebaseService.delete(category.getImage());
        }
        this.categoryRepository.delete(category);

        return SuccessResponse.builder().addMessage("Xóa dữ liệu thành công").returnDeleted();
    }
}
