package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.request.ApplyCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.response.CouponCodeResponse;
import com.hcmute.fneststorebe.domain.entities.CouponCode;
import com.hcmute.fneststorebe.domain.entities.Order;
import com.hcmute.fneststorebe.domain.entities.ShippingCharge;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.CouponCodeRepository;
import com.hcmute.fneststorebe.repository.OrderRepository;
import com.hcmute.fneststorebe.repository.ShippingChargeRepository;
import com.hcmute.fneststorebe.service.interfaces.CouponCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CouponCodeServiceImpl implements CouponCodeService {
    @Autowired
    private CouponCodeRepository couponCodeRepository;

    @Autowired
    private ShippingChargeRepository shippingChargeRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public ResponseBaseAbstract applyCouponCode(ApplyCouponCodeRequest request) {
        Double total = request.getOrderTotal();
        CouponCode couponCode = this.couponCodeRepository.getCouponCodeByCode(request.getCode());
        if (couponCode == null)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Mã giảm giá không tồn tại");
        validateCouponCode(couponCode);
        Double discount = total * couponCode.getValue()/100;
        System.out.println(discount);
        System.out.println(couponCode.getMaxDiscount());
        Double finalDiscount = discount >= couponCode.getMaxDiscount() ? couponCode.getMaxDiscount() : discount;
        total -= finalDiscount;

        Map<String, Object> data = new HashMap<>();
        data.put("discount", finalDiscount);
        data.put("total", total);
        return SuccessResponse.builder()
                .addMessage("Áp mã thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createCouponCode(CreateCouponCodeRequest request) {
        CouponCode couponCode = new CouponCode();
        couponCode.setCode(request.getCode());
        couponCode.setValue(request.getValue());
        couponCode.setDescription(request.getDescription());
        couponCode.setMinOrderValue(request.getMinOrderValue());
        couponCode.setMaxDiscount(request.getMaxDiscount());
        couponCode.setTimes(request.getTimes());
        couponCode.setBeginDate(request.getBeginDate());
        couponCode.setEndDate(request.getEndDate());

        this.couponCodeRepository.save(couponCode);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(new CouponCodeResponse(couponCode))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllCoupon() {
        List<CouponCode> couponCodes = this.couponCodeRepository.findAll();
        List<CouponCodeResponse> couponCodeResponses = couponCodes.stream().map(couponCode -> new CouponCodeResponse(couponCode)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(couponCodeResponses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getCouponCodeById(Integer id) {
        CouponCode couponCode = this.couponCodeRepository.getCouponCodeById(id);
        if (couponCode == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy coupon code với id = "+id);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(new CouponCodeResponse(couponCode))
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateCouponCode(UpdateCouponCodeRequest request) {
        Integer id = request.getId();
        CouponCode couponCode = this.couponCodeRepository.getCouponCodeById(id);
        if (couponCode == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy coupon code với id = "+id);
        if (request.getCode() != null)
            couponCode.setCode(request.getCode());
        if (request.getValue() != null)
            couponCode.setValue(request.getValue());
        if (request.getDescription() != null)
            couponCode.setDescription(request.getDescription());
        if (request.getMinOrderValue() != null)
            couponCode.setMinOrderValue(request.getMinOrderValue());
        if (request.getMaxDiscount() != null)
            couponCode.setMaxDiscount(request.getMaxDiscount());
        if (request.getTimes() != null)
            couponCode.setTimes(request.getTimes());
        if (request.getBeginDate() != null)
            couponCode.setBeginDate(request.getBeginDate());
        if (request.getEndDate() != null)
            couponCode.setEndDate(request.getEndDate());
        this.couponCodeRepository.save(couponCode);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(new CouponCodeResponse(couponCode))
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteCouponCode(Integer id) {
        CouponCode couponCode = this.couponCodeRepository.getCouponCodeById(id);
        if (couponCode == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy coupon code với id = "+id);
        List<Order> orderList = this.orderRepository.getOrderByCouponCode(id);
        if (orderList.size() != 0)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Mã giảm giá đã có đơn hàng sử dụng");
        this.couponCodeRepository.delete(couponCode);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    private void validateCouponCode(CouponCode couponCode) {
        Date beginDate = couponCode.getBeginDate();
        Date endDate = couponCode.getEndDate();
        Date currentDate = new Date();
        if (currentDate.before(beginDate))
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Mã giảm giá chưa thể sử dụng");
        if (currentDate.after(endDate))
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Mã giám giá đã hết hạn sử dụng");
        if (couponCode.getTimes() == 0) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Mã giảm giá đã hết");
        }
    }
}
