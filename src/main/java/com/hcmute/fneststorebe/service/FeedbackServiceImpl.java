package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateFeedbackRequest;
import com.hcmute.fneststorebe.domain.entities.Feedback;
import com.hcmute.fneststorebe.repository.FeedbackRepository;
import com.hcmute.fneststorebe.service.interfaces.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackRepository feedbackRepository;

    @Override
    public ResponseBaseAbstract createFeedback(CreateFeedbackRequest request) {
        Feedback feedback = new Feedback();
        feedback.setFullName(request.getFullName());
        feedback.setEmail(request.getEmail());
        feedback.setContent(request.getContent());
        this.feedbackRepository.save(feedback);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(feedback)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllFeedback() {
        List<Feedback> feedbackList = this.feedbackRepository.findAll();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(feedbackList)
                .returnGetOK();
    }
}
