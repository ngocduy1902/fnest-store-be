package com.hcmute.fneststorebe.service;

import com.google.common.cache.CacheBuilder;
import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.cache.OtpCache;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.internal.LoadingCache;
import org.springframework.stereotype.Service;

@Service
public class OtpService {
    @Autowired
    private MailService mailService;

    @Autowired
    OtpCache otpCache;

    @Autowired
    private UserRepository userRepository;

    public ResponseBaseAbstract sendOtpViaEmail(String email) {
        if (!this.userRepository.existsByEmail(email)) {
            throw new ServiceExceptionFactory().notFound()
                    .addMessage("Không tìm thấy người dùng với địa chỉ email này");
        }
        User user = this.userRepository.getUserByEmail(email);
        sendOtp(user);

        return SuccessResponse.builder().addMessage("OTP đã được gửi").returnGetOK();
    }

    private void sendOtp(User user) {
        String otp = otpCache.create(user.getUsername());
        mailService.sendMailConfirmCode(user, otp);
    }
}
