package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateOrderItemRequest;
import com.hcmute.fneststorebe.domain.dto.request.CreateOrderRequest;
import com.hcmute.fneststorebe.domain.dto.request.ShippingChargeRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateOrderStatusRequest;
import com.hcmute.fneststorebe.domain.dto.response.*;
import com.hcmute.fneststorebe.domain.entities.*;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.*;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import com.hcmute.fneststorebe.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CouponCodeRepository couponCodeRepository;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private ShippingChargeRepository shippingChargeRepository;

    @Autowired
    private OrderItemRepository orderDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Override
    public ResponseBaseAbstract getOrderByUser(User user, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Order> orderPage = this.orderRepository.getOrderByUserId(user.getId(),pageable);
        List<Order> orderList = orderPage.getContent();
        List<OrderResponse> orderResponseList = orderList.stream().map(order -> new OrderResponse(order)).toList();
        for (int i = 0;i < orderList.size();i++) {
            List<OrderItem> list1 = orderList.get(i).getOrderItems();
            List<OrderItemResponse> list2 = list1.stream().map(orderItem -> new OrderItemResponse(orderItem)).toList();
            for (OrderItemResponse response : list2) {
                response.setProductThumbnail(this.firebaseService.getImageUrl(response.getProductThumbnail()));
            }
            orderResponseList.get(i).setResponses(list2);
        }
        PagingAndSortingResponse<OrderResponse> data =
                new PagingAndSortingResponse<>(orderResponseList, currentPage, pageSize, orderPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getOrderDetail(Integer orderId) {
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + orderId);
        CouponCode couponCode = order.getCouponCode();
        DeliveryAddress d = order.getDeliveryAddress();
        CommuneWardTown communeWardTown = d.getCommuneWardTown();
        District district = communeWardTown.getDistrict();
        ProvinceCity provinceCity = district.getProvinceCity();
        String address = d.getAddressDetail() + ", " + communeWardTown.getName()
                + ", " + district.getName() + ", " + provinceCity.getName();
        OrderDetailResponse response = new OrderDetailResponse(order);
        response.setDeliveryAddress(new BuyerDeliveryAddressResponse(d.getId(),
                address,
                d.getReceiverName(),
                d.getReceiverPhone()));
        List<OrderItem> orderItemList = order.getOrderItems();
        Double total = orderItemList.stream().mapToDouble(orderDetail -> orderDetail.getTotal()).sum();
        List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(orderItem -> new OrderItemResponse(orderItem)).toList();
        for (OrderItemResponse resp : orderItemResponses) {
            resp.setProductThumbnail(this.firebaseService.getImageUrl(resp.getProductThumbnail()));
        }
        if (couponCode != null) {
            Double discount = total * couponCode.getValue()/100;
            Double finalDiscount = discount >= couponCode.getMaxDiscount() ? couponCode.getMaxDiscount() : discount;
            response.setCodeDiscount(finalDiscount);
        }
        else
            response.setCodeDiscount(0.0);
        response.setOrderItemList(orderItemResponses);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createOrder(User user, CreateOrderRequest request) {
        Double total=0.0;
        for (CreateOrderItemRequest item : request.getOrderItemList()) {
            Product product = this.productRepository.getProductById(item.getProductId());
            if (product == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy sản phẩm với id = "+item.getProductId());
            Double price = product.isOnSale() ? product.getSalePrice() : product.getPrice();
            total += price;
        }
        Order order = new Order();
        order.setUser(user);
        if (request.getCode() != null) {
            CouponCode couponCode = this.couponCodeRepository.getCouponCodeByCode(request.getCode());
            couponCode.setTimes(couponCode.getTimes() - 1);
            this.couponCodeRepository.save(couponCode);
            Double discount = total * couponCode.getValue()/100;
            Double finalDiscount = discount >= couponCode.getMaxDiscount() ? couponCode.getMaxDiscount() : discount;
            total -= finalDiscount;
            order.setCouponCode(couponCode);
        }
        DeliveryAddress address = this.deliveryAddressRepository.getDeliveryAddressById(request.getDeliveryAddressId());
        order.setDeliveryAddress(address);
        ShippingCharge shippingCharge = this.shippingChargeRepository.getShippingCharge(request.getShippingCharge());
        order.setShippingCharge(shippingCharge);
        total += request.getShippingCharge();
        order.setTotal(total);
        order.setStatus(OrderStatus.PENDING);
        order.setPaymentMethod(request.getPaymentMethod());
        this.orderRepository.save(order);
        for (CreateOrderItemRequest item : request.getOrderItemList()) {
            OrderItem orderItem = new OrderItem();
            Product product = this.productRepository.getProductById(item.getProductId());
            product.setSold(product.getSold() + item.getQuantity());
            if (product.getInStock() <= 0) {
                product.setInStock(0);
                throw ServiceExceptionFactory.badRequest().addMessage("Sản phẩm đã hết hàng !");
            }
            product.setInStock(product.getInStock() - item.getQuantity());
            this.productRepository.save(product);
            orderItem.setProduct(product);
            orderItem.setQuantity(item.getQuantity());
            Double price = product.isOnSale() ? product.getSalePrice() : product.getPrice();
            orderItem.setTotal(item.getQuantity() * price);
            orderItem.setProductMaterial(product.getMaterial());
            orderItem.setProductName(product.getName());
            orderItem.setProductThumbnail(product.getThumbnail());
            orderItem.setProductPrice(price);
            orderItem.setProductSize(product.getSize());
            orderItem.setOrder(order);
            this.orderDetailRepository.save(orderItem);
        }
        return SuccessResponse.builder()
                .addMessage("Tạo đơn hàng thành công")
                .setData(new OrderResponse(order))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract updateOrderStatus(UpdateOrderStatusRequest request) {
        Order order = this.orderRepository.getOrderById(request.getId());
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + request.getId());
        if (request.getStatus().equals(OrderStatus.PENDING))
            order.setStatus(OrderStatus.CONFIRMED);
        else if (request.getStatus().equals(OrderStatus.CONFIRMED))
            order.setStatus(OrderStatus.IN_SHIPPING);
        else
            order.setStatus(OrderStatus.COMPLETED);
        order.setLastUpdatedAt(new Date());
        this.orderRepository.save(order);
        return SuccessResponse.builder()
                .addMessage("Cập nhật trạng thái đơn hàng thành công")
                .setData(new OrderResponse(order))
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract cancelOrder(UpdateOrderStatusRequest request) {
        Order order = this.orderRepository.getOrderById(request.getId());
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + request.getId());
        if (request.getStatus().equals(OrderStatus.PENDING) || request.getStatus().equals(OrderStatus.CONFIRMED)) {
            order.setStatus(OrderStatus.CANCELED);
            order.setLastUpdatedAt(new Date());
            this.orderRepository.save(order);
        }
        else
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Trạng thái đơn hàng không cho phép hủy");
        List<OrderItem> orderItemList = order.getOrderItems();
        List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(orderItem -> new OrderItemResponse(orderItem)).toList();
        for (OrderItem item : orderItemList) {
            Product product = item.getProduct();
            product.setSold(product.getSold() - item.getQuantity());
            product.setInStock(product.getInStock() + item.getQuantity());
            this.productRepository.save(product);
        }
        CouponCode couponCode = order.getCouponCode();
        if (couponCode != null)
        {
            couponCode.setTimes(couponCode.getTimes() + 1);
            this.couponCodeRepository.save(couponCode);
        }
        OrderResponse response = new OrderResponse(order);
        response.setResponses(orderItemResponses);
        return SuccessResponse.builder()
                .addMessage("Hủy đơn hàng thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract shippingCharge(ShippingChargeRequest request) {
        Double shippingCharge = 0.0;
        Double total = request.getTotal();
        List<ShippingCharge> shippingChargeList = this.shippingChargeRepository.findAll();
        for (ShippingCharge charge : shippingChargeList) {
            if (total >= charge.getMinValue() && total <= charge.getMaxValue()) {
                shippingCharge = charge.getCharge();
                break;
            }
        }
        total += shippingCharge;
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("shipping_charge",shippingCharge);
        return SuccessResponse.builder()
                .addMessage("Tính phí vận chuyển thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllOrder() {
        List<Order> orders = this.orderRepository.findAll();
        List<OrderDetailResponse> orderResponseList = orders.stream().map(order -> new OrderDetailResponse(order)).toList();
        for (int i = 0;i < orders.size();i++) {
            CouponCode couponCode = orders.get(i).getCouponCode();
            List<OrderItem> orderItemList = orders.get(i).getOrderItems();
            Double total = orderItemList.stream().mapToDouble(orderDetail -> orderDetail.getTotal()).sum();
            List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(orderItem -> new OrderItemResponse(orderItem)).toList();
            for (OrderItemResponse resp : orderItemResponses) {
                resp.setProductThumbnail(this.firebaseService.getImageUrl(resp.getProductThumbnail()));
            }
            orderResponseList.get(i).setOrderItemList(orderItemResponses);
            DeliveryAddress d = orders.get(i).getDeliveryAddress();
            CommuneWardTown communeWardTown = d.getCommuneWardTown();
            District district = communeWardTown.getDistrict();
            ProvinceCity provinceCity = district.getProvinceCity();
            String address = d.getAddressDetail() + ", " + communeWardTown.getName()
                    + ", " + district.getName() + ", " + provinceCity.getName();
            orderResponseList.get(i).setDeliveryAddress(new BuyerDeliveryAddressResponse(
                    d.getId(),
                    address,
                    d.getReceiverName(),
                    d.getReceiverPhone()
            ));
            if (couponCode != null) {
                Double discount = total * couponCode.getValue()/100;
                Double finalDiscount = discount >= couponCode.getMaxDiscount() ? couponCode.getMaxDiscount() : discount;
                orderResponseList.get(i).setCodeDiscount(finalDiscount);
            }
            else
                orderResponseList.get(i).setCodeDiscount(0.0);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(orderResponseList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getOrderByStatus(OrderStatus status, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Order> orderPage = this.orderRepository.getOrderByStatus(status, pageable);
        List<Order> orderList = orderPage.getContent();
        List<OrderResponse> orderResponseList = orderList.stream().map(order -> new OrderResponse(order)).toList();
        for (int i = 0;i < orderList.size();i++) {
            List<OrderItem> list1 = orderList.get(i).getOrderItems();
            List<OrderItemResponse> list2 = list1.stream().map(orderItem -> new OrderItemResponse(orderItem)).toList();
            for (OrderItemResponse response : list2) {
                response.setProductThumbnail(this.firebaseService.getImageUrl(response.getProductThumbnail()));
            }
            orderResponseList.get(i).setResponses(list2);
        }
        PagingAndSortingResponse<OrderResponse> data =
                new PagingAndSortingResponse<>(orderResponseList, currentPage, pageSize, orderPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }
}
