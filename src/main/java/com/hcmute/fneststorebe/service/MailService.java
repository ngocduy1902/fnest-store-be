package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionBase;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String senderMail;

    @Value("${com.hcmute.fneststorebe.security.otp.expired-after-mins}")
    private long expiredAfterMins;

    @Autowired
    private Configuration freeMakerConfiguration;

    public void sendMailConfirmCode(User user, String otp) {
        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Map<String, Object> model = new HashMap<>();
            model.put("fullName", user.getFullName());
            model.put("otp", otp);
            model.put("expiredAfterMins", expiredAfterMins);

            Template t = freeMakerConfiguration.getTemplate("email-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(user.getEmail());
            helper.setText(html, true);
            helper.setSubject("[Fnest Store] Email confirmation code");
            helper.setFrom(senderMail);

            mailSender.send(message);

        } catch (Exception e) {
            throw new ServiceExceptionBase(HttpStatus.INTERNAL_SERVER_ERROR)
                    .addMessage("Failed to send mail confirmation code (" + e.getMessage() + ")");
        }
    }
}
