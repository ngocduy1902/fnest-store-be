package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateRoomRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateRoomRequest;
import com.hcmute.fneststorebe.domain.dto.response.RoomResponse;
import com.hcmute.fneststorebe.domain.entities.Room;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.RoomRepository;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import com.hcmute.fneststorebe.service.interfaces.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Override
    public ResponseBaseAbstract getAllRoom() {
        List<Room> roomList = this.roomRepository.findAll();
        List<RoomResponse> data = roomList.stream().map(room -> new RoomResponse(room)).toList();
        for (RoomResponse response : data) {
            response.setImage(this.firebaseService.getImageUrl(response.getImage()));
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getRoomById(Integer id) {
        Room room = this.roomRepository.getRoomById(id);
        if (room == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy room với id = " + id);
        }
        RoomResponse data = new RoomResponse(room);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createRoom(CreateRoomRequest request) throws IOException {
        Room room = new Room();
        room.setName(request.getName());
        room.setImage(this.firebaseService.save(request.getImage()));
        this.roomRepository.save(room);
        RoomResponse data = new RoomResponse(room);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateRoom(UpdateRoomRequest request) throws IOException {
        Integer id = request.getId();
        Room room = this.roomRepository.getRoomById(id);
        if (room == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy room với id = " + id);
        }
        room.setName(request.getName());
        if (request.getImage() != null) {
            this.firebaseService.delete(room.getImage());
            room.setImage(this.firebaseService.save(request.getImage()));
        }
        this.roomRepository.save(room);
        RoomResponse data = new RoomResponse(room);
        data.setImage(this.firebaseService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract deleteRoom(Integer id) throws IOException {
        Room room = this.roomRepository.getRoomById(id);
        if (room == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy room với id = " + id);
        }
        if (room.getImage() != null) {
            this.firebaseService.delete(room.getImage());
        }
        this.roomRepository.delete(room);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }
}
