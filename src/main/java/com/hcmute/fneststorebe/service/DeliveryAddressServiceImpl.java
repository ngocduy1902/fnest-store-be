package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.dto.response.BuyerDeliveryAddressResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.entities.*;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.CommuneWardTownRepository;
import com.hcmute.fneststorebe.repository.DeliveryAddressRepository;
import com.hcmute.fneststorebe.repository.UserRepository;
import com.hcmute.fneststorebe.service.interfaces.DeliveryAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeliveryAddressServiceImpl implements DeliveryAddressService {
    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private CommuneWardTownRepository communeWardTownRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseBaseAbstract createDeliveryAddress(User user, CreateDeliveryAddressRequest request) {
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setAddressDetail(request.getAddressDetail());
        deliveryAddress.setReceiverName(request.getReceiverName());
        deliveryAddress.setReceiverPhone(request.getReceiverPhone());

        CommuneWardTown communeWardTown = this.communeWardTownRepository.getCommuneWardTownByName(request.getCommuneWardTownName());
        if (communeWardTown == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Sai tên phường, thị trấn");

        deliveryAddress.setCommuneWardTown(communeWardTown);
        deliveryAddress.setUser(user);

        this.deliveryAddressRepository.save(deliveryAddress);

        if (user.getDefaultAddress() == null) {
            user.setDefaultAddress(deliveryAddress);
            this.userRepository.save(user);
        }

        String address = request.getAddressDetail() + ", " + request.getCommuneWardTownName()
                + ", " + request.getDistrictName() + ", " + request.getProvinceCityName();
        BuyerDeliveryAddressResponse data =
                new BuyerDeliveryAddressResponse(deliveryAddress.getId(), address, request.getReceiverName(), request.getReceiverPhone());
        return SuccessResponse.builder().addMessage("Tạo địa chỉ giao hàng thành công")
                .setData(data)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract updateDeliveryAddress(User user, UpdateDeliveryAddressRequest request) {
        DeliveryAddress deliveryAddress = this.deliveryAddressRepository.getDeliveryAddressById(request.getId());
        if (deliveryAddress == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại địa chỉ với id = " + request.getId());
        if (deliveryAddress.getUser().getId() != user.getId())
            throw ServiceExceptionFactory.forbidden().addMessage("Không có quyền thao tác");

        deliveryAddress.setAddressDetail(request.getAddressDetail());
        deliveryAddress.setReceiverName(request.getReceiverName());
        deliveryAddress.setReceiverPhone(request.getReceiverPhone());

        CommuneWardTown communeWardTown = this.communeWardTownRepository.getCommuneWardTownByName(request.getCommuneWardTownName());
        if (communeWardTown == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Sai tên phường, thị trấn");

        deliveryAddress.setCommuneWardTown(communeWardTown);
        this.deliveryAddressRepository.save(deliveryAddress);
        String address = request.getAddressDetail() + ", " + request.getCommuneWardTownName()
                + ", " + request.getDistrictName() + ", " + request.getProvinceCityName();
        BuyerDeliveryAddressResponse data =
                new BuyerDeliveryAddressResponse(deliveryAddress.getId(), address, request.getReceiverName(), request.getReceiverPhone());
        return SuccessResponse.builder().addMessage("Cập nhật địa chỉ giao hàng thành công")
                .setData(data)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteDeliveryAddress(User user, Integer deliveryAddressId) {
        DeliveryAddress deliveryAddress = this.deliveryAddressRepository.getDeliveryAddressById(deliveryAddressId);
        if (deliveryAddress == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại địa chỉ với id = " + deliveryAddressId);
        if (deliveryAddress.getUser().getId() != user.getId())
            throw ServiceExceptionFactory.forbidden().addMessage("Không có quyền thao tác");
        if (user.getDefaultAddress().getId() == deliveryAddressId) {
            user.setDefaultAddress(null);
            this.userRepository.save(user);
        }
        this.deliveryAddressRepository.delete(deliveryAddress);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract getAllDeliveryAddressByUser(User user) {
        List<DeliveryAddress> deliveryAddresses = this.deliveryAddressRepository.getAllDeliveryAddressByUserId(user.getId());
        List<BuyerDeliveryAddressResponse> data = new ArrayList<>();

        if (deliveryAddresses.size() != 0) {
            for (DeliveryAddress d : deliveryAddresses) {
                CommuneWardTown communeWardTown = d.getCommuneWardTown();
                District district = communeWardTown.getDistrict();
                ProvinceCity provinceCity = district.getProvinceCity();
                String address = d.getAddressDetail() + ", " + communeWardTown.getName()
                        + ", " + district.getName() + ", " + provinceCity.getName();
                BuyerDeliveryAddressResponse response =
                        new BuyerDeliveryAddressResponse(d.getId(), address, d.getReceiverName(), d.getReceiverPhone());

                data.add(response);
            }
        }


        Map<String, Object> responseData = new HashMap<>();
        responseData.put("deliveryAddresses", data);
        responseData.put("defaultAddressId", user.getDefaultAddress() != null ? user.getDefaultAddress().getId() : null);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responseData)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateDefaultAddress(User user, UpdateDefaultAddressRequest request) {
        if (request.getDefaultAddressId() == null) {
            throw ServiceExceptionFactory.badRequest().addMessage("Dữ liệu không hợp lệ");
        }

        DeliveryAddress address = this.deliveryAddressRepository.getDeliveryAddressById(request.getDefaultAddressId());
        if (address == null) {
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy địa chỉ có id = " + request.getDefaultAddressId());
        }

        user.setDefaultAddress(address);
        this.userRepository.save(user);
        Map<String, Object> data = new HashMap<>();
        data.put("defaultAddressId",address.getId());
        return SuccessResponse.builder()
                .addMessage("Cập nhật địa chỉ mặc định thành công")
                .setData(data)
                .returnUpdated();
    }
}
