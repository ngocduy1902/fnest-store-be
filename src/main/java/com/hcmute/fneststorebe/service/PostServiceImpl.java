package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreatePostRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdatePostRequest;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.dto.response.PostResponse;
import com.hcmute.fneststorebe.domain.entities.Post;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.PostRepository;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import com.hcmute.fneststorebe.service.interfaces.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Override
    public ResponseBaseAbstract createPost(CreatePostRequest request) throws IOException {
        Post post = new Post();
        post.setDescription(request.getDescription());
        post.setTitle(request.getTitle());
        List<String> banners = new ArrayList<>();
        List<String> bannerUrls = new ArrayList<>();
        for (MultipartFile banner : request.getBanners()) {
            String bannerName = this.firebaseService.save(banner);
            banners.add(bannerName);
            bannerUrls.add(this.firebaseService.getImageUrl(bannerName));
        }
        post.setBanners(banners);
        postRepository.save(post);
        PostResponse data = new PostResponse(post);
        data.setBanners(bannerUrls);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllPost() {
        List<Post> postList = this.postRepository.findAll();
        List<PostResponse> postResponses = postList.stream().map(post -> new PostResponse(post)).toList();
        for (PostResponse postResponse : postResponses) {
            postResponse.setBanners(postResponse.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList());
        }
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(postResponses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getPostByBannerName(String name) {
        List<Post> postList = this.postRepository.findPostByBanner(name);
        List<PostResponse> postResponses = postList.stream().map(post -> new PostResponse(post)).toList();
        for (PostResponse postResponse : postResponses) {
            postResponse.setBanners(postResponse.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList());
        }
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(postResponses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchPost(Map<String, String> queries) {
        PagingAndSortingResponse<Post> postPage = this.postRepository.searchPost(queries);
        List<Post> postList = postPage.getContent();
        List<PostResponse> postResponses = postList.stream().map(post -> new PostResponse(post)).toList();
        for (PostResponse postResponse : postResponses) {
            postResponse.setBanners(postResponse.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList());
        }
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(postResponses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getPostPage(Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Post> postPage = this.postRepository.getPostPage(pageable);
        List<Post> postList = postPage.getContent();
        List<PostResponse> postResponses = postList.stream().map(post -> new PostResponse(post)).toList();
        for (PostResponse postResponse : postResponses) {
            List<String> banners = postResponse.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList();
            postResponse.setBanners(banners);
        }
        Integer totalPages = postPage.getTotalPages();
        PagingAndSortingResponse<PostResponse> response = new PagingAndSortingResponse<>(
                postResponses, currentPage, pageSize, totalPages
        );
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getPostDetail(Integer id) {
        Post post = this.postRepository.getPostById(id);
        if (post == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy bài viết có id = " + id);
        List<String> bannerUrls = post.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList();
        PostResponse response = new PostResponse(post);
        response.setBanners(bannerUrls);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract deletePost(Integer id) throws IOException {
        Post post = this.postRepository.getPostById(id);
        if (post == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy bài viết có id = " + id);
        for (String bannerName : post.getBanners()) {
            this.firebaseService.delete(bannerName);
        }
        this.postRepository.delete(post);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract updatePost(UpdatePostRequest request) throws IOException {
        Integer id = request.getId();
        Post post = this.postRepository.getPostById(id);
        if (post == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy bài viết có id = " + id);
        post.setDescription(request.getDescription());
        post.setTitle(request.getTitle());
        List<String> bannerNames = new ArrayList<>();
        for (MultipartFile banner : request.getBanners()) {
            String bannerName = this.firebaseService.save(banner);
            bannerNames.add(bannerName);
        }
        for (String bannerName : post.getBanners()) {
            this.firebaseService.delete(bannerName);
        }
        post.setBanners(bannerNames);
        this.postRepository.save(post);
        List<String> bannerUrls = post.getBanners().stream().map(banner -> this.firebaseService.getImageUrl(banner)).toList();
        PostResponse postResponse = new PostResponse(post);
        postResponse.setBanners(bannerUrls);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(postResponse)
                .returnUpdated();
    }
}
