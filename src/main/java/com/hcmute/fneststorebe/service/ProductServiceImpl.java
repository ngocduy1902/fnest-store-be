package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateProductRequest;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.dto.response.ProductResponse;
import com.hcmute.fneststorebe.domain.entities.*;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.*;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import com.hcmute.fneststorebe.service.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public ResponseBaseAbstract getAllProduct() {
        List<Product> productList = this.productRepository.findAll();
        List<ProductResponse> data = productList.stream().map(
                product -> new ProductResponse(product)
        ).toList();

        setImageUrls(data);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getProductById(Integer id) {
        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + id);
        }
        ProductResponse data = new ProductResponse(product);
        List<ProductImage> images = this.productImageRepository.getProductImageByProductId(id);
        List<String> imageUrls = new ArrayList<>();
        for (ProductImage image : images) {
            String imageName = image.getSource();
            String imageUrl = this.firebaseService.getImageUrl(imageName);
            imageUrls.add(imageUrl);
        }
        data.setThumbnail(this.firebaseService.getImageUrl(data.getThumbnail()));
        data.setImageUrls(imageUrls);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createProduct(CreateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException {
        Product product = new Product();
        product.setName(request.getName());
        product.setPrice(request.getPrice());
        if (request.getSalePrice() != null) {
            product.setSalePrice(request.getSalePrice());
            product.setOnSale(true);
        }
        else
            product.setOnSale(false);
        product.setDescription(request.getDescription());
        product.setSize(request.getSize());
        product.setMaterial(request.getMaterial());
        product.setSold(0);
        product.setInStock(request.getInStock());
        product.setFeatured(request.isFeatured());
        product.setTotalReviewPoint(0);
        product.setReviewAmmount(0);
        if (request.getCategoryId() != null) {
            Category category = this.categoryRepository.getCategoryById(request.getCategoryId());
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + request.getCategoryId());
            product.setCategory(category);
        }

        if (request.getCollectionId() != null) {
            Collection collection = this.collectionRepository.getCollectionById(request.getCollectionId());
            if (collection == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy collection với id = " + request.getCollectionId());
            product.setCollection(collection);
        }

        String thumbnailName = this.firebaseService.save(thumbnail);
        product.setThumbnail(thumbnailName);

        this.productRepository.save(product);

        List<String> imageUrls = new ArrayList<>();
        for (MultipartFile image : images) {
            ProductImage productImage = new ProductImage();
            String imageName = this.firebaseService.save(image);
            productImage.setSource(imageName);
            productImage.setProduct(product);
            this.productImageRepository.save(productImage);
            imageUrls.add(this.firebaseService.getImageUrl(imageName));
        }

        ProductResponse data = new ProductResponse(product);
        data.setImageUrls(imageUrls);
        data.setThumbnail(firebaseService.getImageUrl(data.getThumbnail()));
        return SuccessResponse.builder().addMessage("Tạo dữ liệu thành công").setData(data).returnCreated();
    }

    @Override
    public ResponseBaseAbstract getProductByCategoryName(String categoryName, Integer currentPage, Integer pageSize, String sort) {
        if (!this.categoryRepository.existByName(categoryName))
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tồn tại category có tên là: " + categoryName);

        Pageable pageable;
        if (sort == null)
            pageable = PageRequest.of(currentPage, pageSize, Sort.by("id").ascending());
        else
        {
            String[] tokens = sort.split("\\.");
            String sortColumn = tokens[0];
            String sortType = tokens[1];
            if (sortType.equalsIgnoreCase("DESC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).descending());
            else if (sortType.equalsIgnoreCase("ASC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).ascending());
            else
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Sort type không hợp lệ");
        }

        Page<Product> productPage = this.productRepository.getProductByCategoryName(categoryName, pageable);
        List<Product> productList = productPage.getContent();
        List<ProductResponse> productResponse = productList.stream().map(product -> new ProductResponse(product)).toList();
        setImageUrls(productResponse);
        Integer totalPages = productPage.getTotalPages();
        PagingAndSortingResponse<ProductResponse> response =
                new PagingAndSortingResponse<>(productResponse, currentPage, pageSize, totalPages);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response).returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchAndFilterProduct(Map<String, String> queries) {
        PagingAndSortingResponse<Product> productPage = this.productRepository.searchProduct(queries);
        List<ProductResponse> productResponses = productPage.getContent().stream().map(product -> new ProductResponse(product)).toList();
        setImageUrls(productResponses);
        PagingAndSortingResponse<ProductResponse> data =
                new PagingAndSortingResponse<>(productResponses, productPage.getCurrentPage(), productPage.getPageSize(), productPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getProductByCollectionId(Integer collectionId, Integer currentPage, Integer pageSize, String sort) {
        if (!this.collectionRepository.existsById(collectionId))
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tồn tại collection với  là: " + collectionId);

        Pageable pageable;
        if (sort == null)
            pageable = PageRequest.of(currentPage, pageSize, Sort.by("id").ascending());
        else
        {
            String[] tokens = sort.split("\\.");
            String sortColumn = tokens[0];
            String sortType = tokens[1];
            if (sortType.equalsIgnoreCase("DESC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).descending());
            else if (sortType.equalsIgnoreCase("ASC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).ascending());
            else
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Sort type không hợp lệ");
        }

        Page<Product> productPage = this.productRepository.getProductByCollectionId(collectionId, pageable);
        List<Product> productList = productPage.getContent();
        List<ProductResponse> productResponse = productList.stream().map(product -> new ProductResponse(product)).toList();
        setImageUrls(productResponse);
        Integer totalPages = productPage.getTotalPages();
        PagingAndSortingResponse<ProductResponse> response =
                new PagingAndSortingResponse<>(productResponse, currentPage, pageSize, totalPages);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response).returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateProduct(UpdateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException {
        Integer id = request.getId();
        Product product = this.productRepository.getProductById(id);
        if (product == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + id);
        if (request.getCategoryId() != null) {
            Category category = this.categoryRepository.getCategoryById(request.getCategoryId());
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + request.getCategoryId());
            product.setCategory(category);
        }

        if (request.getCollectionId() != null) {
            Collection collection = this.collectionRepository.getCollectionById(request.getCollectionId());
            if (collection == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy collection với id = " + request.getCollectionId());
            product.setCollection(collection);
        }
        product.setName(request.getName());
        product.setPrice(request.getPrice());
        if (request.getSalePrice() != null) {
            product.setSalePrice(request.getSalePrice());
            product.setOnSale(true);
        }
        else
            product.setOnSale(false);
        product.setDescription(request.getDescription());
        product.setSize(request.getSize());
        product.setMaterial(request.getMaterial());
        product.setFeatured(request.isFeatured());

        if (thumbnail != null) {
            this.firebaseService.delete(product.getThumbnail());
            product.setThumbnail(this.firebaseService.save(thumbnail));
        }
        this.productRepository.save(product);
        List<String> imageUrls = new ArrayList<>();
        List<ProductImage> productImageList = this.productImageRepository.getProductImageByProductId(id);
        if (images.size() != 0) {
            if (productImageList.size() != images.size())
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Số lượng ảnh tải lên không hợp lệ");
            for (int i = 0;i < images.size();i++) {
                String imageName = this.firebaseService.save(images.get(i));
                this.firebaseService.delete(productImageList.get(i).getSource());
                productImageList.get(i).setSource(imageName);
                this.productImageRepository.save(productImageList.get(i));
                String imageUrl = this.firebaseService.getImageUrl(imageName);
                imageUrls.add(imageUrl);
            }
        }
        else {
            imageUrls = productImageList.stream().map(productImage ->
                    this.firebaseService.getImageUrl(productImage.getSource())).toList();
        }
        ProductResponse data = new ProductResponse(product);
        data.setThumbnail(this.firebaseService.getImageUrl(data.getThumbnail()));
        data.setImageUrls(imageUrls);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract deleteProduct(Integer id) throws IOException {
        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + id);
        }
        this.firebaseService.delete(product.getThumbnail());
        List<ProductImage> images = this.productImageRepository.getProductImageByProductId(id);
        for (ProductImage image : images) {
            this.firebaseService.delete(image.getSource());
        }
        OrderItem orderItem = this.orderItemRepository.getOrderItemByProductId(id);
        if (orderItem != null) {
            orderItem.setProduct(null);
            this.orderItemRepository.save(orderItem);
        }
        this.productRepository.delete(product);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract getFeaturedProductPage(Integer currentPage, Integer pageSize, String sort) {
        Pageable pageable;
        if (sort == null)
            pageable = PageRequest.of(currentPage, pageSize, Sort.by("id").ascending());
        else
        {
            String[] tokens = sort.split("\\.");
            String sortColumn = tokens[0];
            String sortType = tokens[1];
            if (sortType.equalsIgnoreCase("DESC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).descending());
            else if (sortType.equalsIgnoreCase("ASC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).ascending());
            else
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Sort type không hợp lệ");
        }
        Page<Product> featuredProductPage = this.productRepository.getFeaturedProductPage(pageable);
        List<Product> productList = featuredProductPage.getContent();
        List<ProductResponse> productResponse = productList.stream().map(product -> new ProductResponse(product)).toList();
        setImageUrls(productResponse);
        Integer totalPages = featuredProductPage.getTotalPages();
        PagingAndSortingResponse<ProductResponse> response =
                new PagingAndSortingResponse<>(productResponse, currentPage, pageSize, totalPages);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response).returnGetOK();
    }

    private void setImageUrls(List<ProductResponse> productResponseList) {
        for (ProductResponse response : productResponseList) {
            response.setThumbnail(this.firebaseService.getImageUrl(response.getThumbnail()));
            List<ProductImage> productImages = this.productImageRepository.getProductImageByProductId(response.getId());
            List<String> imageUrls = new ArrayList<>();
            for (ProductImage image : productImages) {
                imageUrls.add(this.firebaseService.getImageUrl(image.getSource()));
            }
            response.setImageUrls(imageUrls);
        }
    }
}
