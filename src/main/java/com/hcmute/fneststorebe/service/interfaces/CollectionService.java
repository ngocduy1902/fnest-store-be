package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCollectionRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCollectionRequest;

import java.io.IOException;

public interface CollectionService {
    ResponseBaseAbstract createCollection(CreateCollectionRequest request) throws IOException;

    ResponseBaseAbstract getAllCollection();

    ResponseBaseAbstract getCollectionById(Integer id);

    ResponseBaseAbstract updateCollection(UpdateCollectionRequest request) throws IOException;

    ResponseBaseAbstract deleteCollection(Integer id) throws IOException;
}
