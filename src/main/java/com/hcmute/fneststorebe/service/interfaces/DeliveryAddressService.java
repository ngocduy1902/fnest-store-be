package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.fneststorebe.domain.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.fneststorebe.domain.entities.User;

public interface DeliveryAddressService {
    ResponseBaseAbstract createDeliveryAddress(User user, CreateDeliveryAddressRequest request);

    ResponseBaseAbstract updateDeliveryAddress(User user, UpdateDeliveryAddressRequest request);

    ResponseBaseAbstract deleteDeliveryAddress(User user, Integer deliveryAddressId);

    ResponseBaseAbstract getAllDeliveryAddressByUser(User user);

    ResponseBaseAbstract updateDefaultAddress(User user, UpdateDefaultAddressRequest request);
}
