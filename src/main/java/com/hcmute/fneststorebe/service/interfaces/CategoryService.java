package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCategoryRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCategoryRequest;

import java.io.IOException;

public interface CategoryService {
    ResponseBaseAbstract getCategoryByRoomId(Integer id);

    ResponseBaseAbstract createCategory(CreateCategoryRequest request) throws IOException;

    ResponseBaseAbstract getAllCategory();

    ResponseBaseAbstract getCategoryById(Integer id);

    ResponseBaseAbstract updateCategory(UpdateCategoryRequest request) throws IOException;

    ResponseBaseAbstract deleteCategory(Integer id) throws IOException;
}
