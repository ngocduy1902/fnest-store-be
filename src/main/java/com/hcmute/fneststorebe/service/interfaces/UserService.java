package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.EmailConfirmationRequest;
import com.hcmute.fneststorebe.domain.dto.request.LoginRequest;
import com.hcmute.fneststorebe.domain.dto.request.*;
import com.hcmute.fneststorebe.domain.entities.User;

import java.util.Map;

public interface UserService {
    ResponseBaseAbstract getAllUser();

    ResponseBaseAbstract createUser(CreateUserRequest request);

    ResponseBaseAbstract registerAdminAccount();

    ResponseBaseAbstract registerUser(UserRegisterRequest request);

    ResponseBaseAbstract authenticate(LoginRequest request);

    ResponseBaseAbstract searchUser(Map<String, String> queries);

    ResponseBaseAbstract confirmClientEmail(User user, EmailConfirmationRequest request);

    ResponseBaseAbstract getProfile(User user);

    ResponseBaseAbstract updateProfile(User user, UpdateProfileRequest request);

    ResponseBaseAbstract updatePassword(User user, UpdatePasswordRequest request);

    ResponseBaseAbstract restorePassword(OtpConfirmRequest request);

    ResponseBaseAbstract updateUserStatus(Integer id);
}
