package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.request.ApplyCouponCodeRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCouponCodeRequest;

public interface CouponCodeService {
    ResponseBaseAbstract applyCouponCode(ApplyCouponCodeRequest request);

    ResponseBaseAbstract createCouponCode(CreateCouponCodeRequest request);

    ResponseBaseAbstract getAllCoupon();

    ResponseBaseAbstract getCouponCodeById(Integer id);

    ResponseBaseAbstract updateCouponCode(UpdateCouponCodeRequest request);

    ResponseBaseAbstract deleteCouponCode(Integer id);
}
