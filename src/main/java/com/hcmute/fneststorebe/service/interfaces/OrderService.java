package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateOrderRequest;
import com.hcmute.fneststorebe.domain.dto.request.ShippingChargeRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateOrderStatusRequest;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;

public interface OrderService {
    ResponseBaseAbstract getOrderByUser(User user, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getOrderDetail(Integer orderId);

    ResponseBaseAbstract createOrder(User user, CreateOrderRequest request);

    ResponseBaseAbstract updateOrderStatus(UpdateOrderStatusRequest request);

    ResponseBaseAbstract cancelOrder(UpdateOrderStatusRequest request);

    ResponseBaseAbstract shippingCharge(ShippingChargeRequest request);

    ResponseBaseAbstract getAllOrder();

    ResponseBaseAbstract getOrderByStatus(OrderStatus status, Integer currentPage, Integer pageSize);
}
