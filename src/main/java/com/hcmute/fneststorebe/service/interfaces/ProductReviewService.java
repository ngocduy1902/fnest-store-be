package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductReviewRequest;
import com.hcmute.fneststorebe.domain.entities.User;

public interface ProductReviewService {
    ResponseBaseAbstract createReview(User user, CreateProductReviewRequest request);

    ResponseBaseAbstract getAllReviewByProduct(String productName, Integer currentPage, Integer pageSize);
}
