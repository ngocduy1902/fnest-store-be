package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;


public interface StatisticService {
    ResponseBaseAbstract getStoreStatistic(Integer month, Integer year);
}
