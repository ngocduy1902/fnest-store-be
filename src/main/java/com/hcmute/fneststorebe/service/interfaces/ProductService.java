package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateProductRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductService {
    ResponseBaseAbstract getAllProduct();

    ResponseBaseAbstract getProductById(Integer id);

    ResponseBaseAbstract createProduct(CreateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException;

    ResponseBaseAbstract getProductByCategoryName(String categoryName, Integer currentPage, Integer pageSize, String sort);

    ResponseBaseAbstract searchAndFilterProduct(Map<String, String> queries);

    ResponseBaseAbstract getProductByCollectionId(Integer collectionId, Integer currentPage, Integer pageSize, String sort);

    ResponseBaseAbstract updateProduct(UpdateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException;

    ResponseBaseAbstract deleteProduct(Integer id) throws IOException;

    ResponseBaseAbstract getFeaturedProductPage(Integer currentPage, Integer pageSize, String sort);
}
