package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreatePostRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdatePostRequest;

import java.io.IOException;
import java.util.Map;

public interface PostService {
    ResponseBaseAbstract createPost(CreatePostRequest request) throws IOException;

    ResponseBaseAbstract getAllPost();

    ResponseBaseAbstract getPostByBannerName(String name);

    ResponseBaseAbstract searchPost(Map<String, String> queries);

    ResponseBaseAbstract getPostPage(Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getPostDetail(Integer id);

    ResponseBaseAbstract deletePost(Integer id) throws IOException;

    ResponseBaseAbstract updatePost(UpdatePostRequest request) throws IOException;
}
