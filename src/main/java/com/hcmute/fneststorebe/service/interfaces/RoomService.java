package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateRoomRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateRoomRequest;

import java.io.IOException;

public interface RoomService {
    ResponseBaseAbstract getAllRoom();

    ResponseBaseAbstract getRoomById(Integer id);

    ResponseBaseAbstract createRoom(CreateRoomRequest request) throws IOException;

    ResponseBaseAbstract updateRoom(UpdateRoomRequest request) throws IOException;

    ResponseBaseAbstract deleteRoom(Integer id) throws IOException;
}
