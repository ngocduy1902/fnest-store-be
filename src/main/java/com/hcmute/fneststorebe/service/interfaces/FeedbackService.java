package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.dto.request.CreateFeedbackRequest;

public interface FeedbackService {
    ResponseBaseAbstract createFeedback(CreateFeedbackRequest request);

    ResponseBaseAbstract getAllFeedback();
}
