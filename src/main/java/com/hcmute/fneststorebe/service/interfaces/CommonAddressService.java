package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;

public interface CommonAddressService {
    ResponseBaseAbstract getAllProvinceCity();

    ResponseBaseAbstract getDistrictByProvinceCityName(String name);

    ResponseBaseAbstract getCommuneWardTownByDistrictName(String name);
}
