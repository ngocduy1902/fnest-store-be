package com.hcmute.fneststorebe.service.interfaces;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FirebaseService {
    String getImageUrl(String name);

    String save(MultipartFile file) throws IOException;

    ResponseBaseAbstract uploadImage(MultipartFile file) throws IOException;

    void delete(String name) throws IOException;
}
