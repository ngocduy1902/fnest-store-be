package com.hcmute.fneststorebe.service;


import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.response.*;
import com.hcmute.fneststorebe.domain.entities.Order;
import com.hcmute.fneststorebe.domain.entities.OrderItem;
import com.hcmute.fneststorebe.domain.entities.Product;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.repository.OrderRepository;
import com.hcmute.fneststorebe.repository.ProductRepository;
import com.hcmute.fneststorebe.repository.UserRepository;
import com.hcmute.fneststorebe.service.interfaces.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class StatisticServiceImpl implements StatisticService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public ResponseBaseAbstract getStoreStatistic(Integer month, Integer year) {
        StatisticResponse data = new StatisticResponse();
        List<User> userList = this.userRepository.findAll();
        data.setNumOfUser(userList.size());
        List<NumOfOrderByDayResponse> responses = new ArrayList<>();
        List<IncomeByDayResponse> responses1 = new ArrayList<>();
        if (month != null && year != null) {
            Double income = 0.0;
            Date firstDate = getFirstDateOfMonth(year, month);
            Date lastDate = getLastDateOfMonth(year, month);
            data.setNumOfOrderByMonth(this.orderRepository.getNumOfOrderByMonth(lastDate, firstDate));
            Date currentDate = new Date();
            int currentMonth = getMonthFromDateUsingLocalDate(currentDate);
            List<Date> dates;
            List<Order> orderList;
            if (month == currentMonth){
                dates = getDatesBetween(firstDate, currentDate);
                orderList = this.orderRepository.getOrderByMonth(currentDate,firstDate);
            }
            else {
                dates = getDatesBetween(firstDate, lastDate);
                orderList = this.orderRepository.getOrderByMonth(lastDate,firstDate);
            }
            for (Date date : dates) {
                Double incomeByDay=0.0;
                NumOfOrderByDayResponse response = new NumOfOrderByDayResponse();
                Integer amount = this.orderRepository.getNumOfOrderByDay(date);
                response.setDate(date);
                response.setAmount(amount);
                responses.add(response);

                IncomeByDayResponse response1 = new IncomeByDayResponse();
                List<Order> orders = this.orderRepository.getOrderByDay(date);
                List<Order> completedOrders = orders.stream()
                        .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED) || order.getStatus().equals(OrderStatus.REVIEWED))
                        .toList();
                if (completedOrders.size() != 0)
                    incomeByDay = completedOrders.stream().mapToDouble(order -> order.getTotal()).sum();
                response1.setDate(date);
                response1.setIncomeByDay(incomeByDay);
                responses1.add(response1);
            }
            data.setNumOfOrderByDays(responses);
            data.setIncomeByDays(responses1);

            List<Order> completedOrdersByMonth = orderList.stream()
                    .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED) || order.getStatus().equals(OrderStatus.REVIEWED))
                    .toList();
            if (completedOrdersByMonth.size() != 0) {
                Map<Integer, Integer> productStatistic = new HashMap<>();
                List<SoldByProductResponse> soldByProductResponses = new ArrayList<>();
                income = completedOrdersByMonth.stream().mapToDouble(order -> order.getTotal()).sum();
                for (Order o : completedOrdersByMonth) {
                    List<OrderItem> orderItemList = o.getOrderItems();
                    for (OrderItem orderItem : orderItemList) {
                        productStatistic.putIfAbsent(orderItem.getProduct().getId(),0);
                    }
                    for (OrderItem orderItem : orderItemList) {
                        Integer productId = orderItem.getProduct().getId();
                        Integer soldCount = productStatistic.get(productId);
                        soldCount += orderItem.getQuantity();
                        productStatistic.replace(productId, soldCount);
                    }
                }
                for (Map.Entry<Integer, Integer> entry : productStatistic.entrySet()) {
                    SoldByProductResponse soldByProductResponse = new SoldByProductResponse(entry.getKey(),entry.getValue());
                    soldByProductResponses.add(soldByProductResponse);
                }
                Integer maxSoldProductId = findMaxKey(productStatistic);
                Product maxSoldProductByMonth = this.productRepository.getProductById(maxSoldProductId);
                data.setProductOfTheMonth(new ProductResponse(maxSoldProductByMonth));
                data.setSoldByProductResponses(soldByProductResponses);
            }
            else {
                data.setProductOfTheMonth(null);
                data.setSoldByProductResponses(new ArrayList<>());
            }

            data.setIncome(income);
            Integer numberOfSoldProduct = 0;
            for (Order order : completedOrdersByMonth) {
                List<OrderItem> orderItemList = order.getOrderItems();
                if (orderItemList.size() != 0)
                {
                    Integer totalQuantity = orderItemList.stream().mapToInt(orderItem -> orderItem.getQuantity()).sum();
                    numberOfSoldProduct += totalQuantity;
                }
            }
            data.setSoldProduct(numberOfSoldProduct);
        }
        else {
            data.setIncome(0.0);
            data.setNumOfOrderByDays(responses);
            data.setIncomeByDays(responses1);
            data.setNumOfOrderByMonth(0);
            data.setSoldProduct(0);
            data.setSoldByProductResponses(new ArrayList<>());
            data.setProductOfTheMonth(null);
        }

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    private static Date getFirstDateOfMonth(int year, int month) {
        LocalDate localDate = LocalDate.of(year, month, 1);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private static Date getLastDateOfMonth(int year, int month) {
        LocalDate lastDayOfMonth = LocalDate.of(year, month, 1)
                .plusMonths(1)
                .minusDays(1);
        return Date.from(lastDayOfMonth.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static List<Date> getDatesBetween(Date startDate, Date endDate) {
        List<Date> dates = new ArrayList<>();
        LocalDate startLocalDate = startDate.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
        LocalDate endLocalDate = endDate.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();

        while (!startLocalDate.isAfter(endLocalDate)) {
            dates.add(java.sql.Date.valueOf(startLocalDate));
            startLocalDate = startLocalDate.plusDays(1);
        }
        return dates;
    }

    private static int getMonthFromDateUsingLocalDate(Date date) {
        LocalDate localDate = date.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
        return localDate.getMonthValue();
    }

    private static Integer findMaxKey(Map<Integer, Integer> map) {
        Integer maxKey = Integer.MIN_VALUE;
        Integer maxValue = Integer.MIN_VALUE;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer currentValue = entry.getValue();
            if (currentValue > maxValue) {
                maxValue = currentValue;
                maxKey = entry.getKey();
            }
        }
        return maxKey;
    }

}
