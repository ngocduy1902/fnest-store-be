package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateCollectionRequest;
import com.hcmute.fneststorebe.domain.dto.request.UpdateCollectionRequest;
import com.hcmute.fneststorebe.domain.dto.response.CollectionResponse;
import com.hcmute.fneststorebe.domain.entities.Collection;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.CollectionRepository;
import com.hcmute.fneststorebe.service.interfaces.CollectionService;
import com.hcmute.fneststorebe.service.interfaces.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService {
    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    private FirebaseService firebaseService;

    @Override
    public ResponseBaseAbstract createCollection(CreateCollectionRequest request) throws IOException {
        Collection collection = new Collection();
        collection.setName(request.getName());
        collection.setDescription(request.getDescription());
        collection.setImage(this.firebaseService.save(request.getImage()));
        this.collectionRepository.save(collection);
        CollectionResponse data = new CollectionResponse(collection);
        data.setImageUrl(this.firebaseService.getImageUrl(data.getImageUrl()));
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllCollection() {
        List<Collection> collectionList = this.collectionRepository.findAll();

        List<CollectionResponse> data = collectionList.stream().map(collection -> new CollectionResponse(collection)).toList();
        for (CollectionResponse response : data)
            response.setImageUrl(this.firebaseService.getImageUrl(response.getImageUrl()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getCollectionById(Integer id) {
        Collection collection = this.collectionRepository.getCollectionById(id);
        if (collection == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy collection với id = " + id);

        CollectionResponse data = new CollectionResponse(collection);
        data.setImageUrl(this.firebaseService.getImageUrl(data.getImageUrl()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateCollection(UpdateCollectionRequest request) throws IOException {
        Collection collection = this.collectionRepository.getCollectionById(request.getId());
        if (collection == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy collection với id = " + request.getId());
        collection.setName(request.getName());
        collection.setDescription(request.getDescription());
        this.firebaseService.delete(collection.getImage());
        collection.setImage(this.firebaseService.save(request.getImage()));
        this.collectionRepository.save(collection);

        CollectionResponse data = new CollectionResponse(collection);
        data.setImageUrl(this.firebaseService.getImageUrl(data.getImageUrl()));
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract deleteCollection(Integer id) throws IOException {
        Collection collection = this.collectionRepository.getCollectionById(id);
        if (collection == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy collection với id = " + id);
        this.firebaseService.delete(collection.getImage());
        this.collectionRepository.delete(collection);
        return SuccessResponse.builder().addMessage("Xóa dữ liệu thành công").returnDeleted();
    }
}
