package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.entities.CommuneWardTown;
import com.hcmute.fneststorebe.domain.entities.District;
import com.hcmute.fneststorebe.domain.entities.ProvinceCity;
import com.hcmute.fneststorebe.repository.CommuneWardTownRepository;
import com.hcmute.fneststorebe.repository.DistrictRepository;
import com.hcmute.fneststorebe.repository.ProvinceCityRepository;
import com.hcmute.fneststorebe.service.interfaces.CommonAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonAddressServiceImpl implements CommonAddressService {
    @Autowired
    private ProvinceCityRepository provinceCityRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private CommuneWardTownRepository communeWardTownRepository;

    @Override
    public ResponseBaseAbstract getAllProvinceCity() {
        List<ProvinceCity> provinceCityList = this.provinceCityRepository.findAll();
        List<String> provinceCityNameList = provinceCityList.stream().map(provinceCity -> provinceCity.getName()).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(provinceCityNameList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getDistrictByProvinceCityName(String name) {
        List<District> districtList = this.districtRepository.getAllDistrictsByProvinceCityName(name);
        List<String> districtNameList = districtList.stream().map(district -> district.getName()).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(districtNameList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getCommuneWardTownByDistrictName(String name) {
        List<CommuneWardTown> communeWardTownList = this.communeWardTownRepository.getAllCommuneWardTownByDistrictName(name);
        List<String> communeNameList = communeWardTownList.stream().map(communeWardTown -> communeWardTown.getName()).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(communeNameList)
                .returnGetOK();
    }
}
