package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.cache.OtpCache;
import com.hcmute.fneststorebe.domain.dto.request.*;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.dto.response.UserResponse;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.UserGender;
import com.hcmute.fneststorebe.domain.enums.UserRole;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.jwt.JwtTokenProvider;
import com.hcmute.fneststorebe.repository.UserRepository;
import com.hcmute.fneststorebe.service.interfaces.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepositories;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private OtpCache otpCache;

    @Override
    public ResponseBaseAbstract getAllUser() {
        List<User> userList = this.userRepositories.findAll();
        List<UserResponse> data = userList.stream().map(user -> new UserResponse(user)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createUser(CreateUserRequest request) {
        if (request.getUsername() == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Username không được bỏ trống");

        if (this.userRepositories.existsByUsername(request.getUsername()))
            throw ServiceExceptionFactory.duplicate().addMessage("Username tồn tại!");

        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        user.setFullName(request.getDisplayName());
        user.setRole(request.getRole());
        user.setStatus(true);
        this.userRepositories.save(user);
        return SuccessResponse.builder()
                .addMessage("Đăng ký tài khoản thành công")
                .setData(new UserResponse(user))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract registerAdminAccount() {
        if (this.userRepositories.existsByUsername("admin"))
            throw ServiceExceptionFactory.duplicate().addMessage("Đã có tài khoản admin!");

        User user = new User();
        user.setUsername("admin");
        user.setPassword(this.passwordEncoder.encode("admin"));
        user.setRole(UserRole.ADMIN);
        user.setFullName("System Admin");
        user.setGender(UserGender.MALE);
        user.setEmailConfirmed(true);
        user.setStatus(true);
        this.userRepositories.save(user);
        return SuccessResponse.builder()
                .addMessage("Tạo tài khoản admin thành công")
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract registerUser(UserRegisterRequest request) {
        if (request.getUsername() == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Username không được bỏ trống");

        if (this.userRepositories.existsByUsername(request.getUsername()))
            throw ServiceExceptionFactory.duplicate().addMessage("Username tồn tại!");

        if (this.userRepositories.existsByEmail(request.getEmail()))
            throw ServiceExceptionFactory.duplicate().addMessage("Email tồn tại!");

        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        user.setFullName(request.getFullName());
        user.setRole(UserRole.BUYER);
        user.setGender(request.getGender());
        user.setEmail(request.getEmail());
        user.setBirthday(request.getBirthday());
        user.setStatus(true);
        user.setEmailConfirmed(false);

        this.userRepositories.save(user);

        return SuccessResponse.builder()
                .addMessage("Đăng ký tài khoản thành công")
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract authenticate(LoginRequest request) {
        User user = this.userRepositories.getUserByUsername(request.getUsername());

        if (user == null) {
            throw ServiceExceptionFactory.unauthorized()
                    .addMessage("Tên đăng nhập hoặc mật khẩu không đúng");
        }



        if (!this.passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw ServiceExceptionFactory.unauthorized()
                    .addMessage("Tên đăng nhập hoặc mật khẩu không đúng");
            }

        String accessToken = this.jwtTokenProvider.generateToken(user);

        Map<String, Object> data = new HashMap<>();
        data.put("user", new UserResponse(user));
        data.put("accessToken", accessToken);
        return SuccessResponse.builder()
                .addMessage("Đăng nhập thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchUser(Map<String, String> queries){
        PagingAndSortingResponse<User> pagingList = this.userRepositories.searchUser(queries);
        List<User> userList = pagingList.getContent().stream().toList();
        List<UserResponse> userResponseList= userList.stream().map(user -> new UserResponse(user)).toList();
        PagingAndSortingResponse<UserResponse> pagingResponse = new PagingAndSortingResponse<UserResponse>(userResponseList, pagingList.getCurrentPage(), pagingList.getPageSize(), pagingList.getTotalPages());
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(pagingResponse)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract confirmClientEmail(User user, EmailConfirmationRequest request) {
        String otpCode = request.getOtpCode();
        String username = user.getUsername();
        if (username.equals(""))
            throw ServiceExceptionFactory.badRequest().addMessage("Thiếu trường username");
        if (!this.userRepositories.existsByUsername(username))
            throw ServiceExceptionFactory.badRequest().addMessage("Username không tồn tại");
        if (otpCode.equals(""))
            throw ServiceExceptionFactory.badRequest().addMessage("Thiếu trường otpCode");
        if (!otpCode.equals(this.otpCache.getOtpCode(username)))
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");

        user.setEmailConfirmed(true);
        user.setLastUpdatedAt(new Date());
        this.userRepositories.save(user);

        return SuccessResponse.builder().addMessage("Xác thực email thành công")
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getProfile(User user) {
        UserResponse response = new UserResponse(user);
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateProfile(User user, UpdateProfileRequest request) {
        String fullName = request.getFullName();
        String email = request.getEmail();
        UserGender gender = request.getGender();
        Date birthday = request.getBirthday();

        if (!user.getEmail().equals(email))
            user.setEmailConfirmed(false);
        user.setFullName(fullName);
        user.setEmail(email);
        user.setGender(gender);
        user.setBirthday(birthday);

        this.userRepositories.save(user);

        return SuccessResponse.builder().addMessage("Cập nhật thông tin thành công")
                .setData(new UserResponse(user)).returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updatePassword(User user, UpdatePasswordRequest request) {
        if (StringUtils.isEmpty(request.getOldPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập mật khẩu cũ");
        if (StringUtils.isEmpty(request.getNewPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập mật khẩu mới");

        String oldPassword = request.getOldPassword();
        String newPassword = request.getNewPassword();

        if (!this.passwordEncoder.matches(oldPassword, user.getPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Mật khẩu cũ không chính xác");

        user.setPassword(this.passwordEncoder.encode(newPassword));
        this.userRepositories.save(user);

        return SuccessResponse.builder().addMessage("Cập nhật mật khẩu thành công")
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract restorePassword(OtpConfirmRequest request) {
        if (StringUtils.isEmpty(request.getUsername()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập username");
        if (StringUtils.isEmpty(request.getOtpCode()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập otp");

        String username = request.getUsername();
        String otpCode = request.getOtpCode();
        User user = this.userRepositories.getUserByUsername(username);

        if (user == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại user với username = " + username);
        if (!user.isEmailConfirmed())
            throw ServiceExceptionFactory.badRequest().addMessage("Tài khoản của bạn chưa xác thực email");

        if (!otpCode.equals(this.otpCache.getOtpCode(username)))
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");

        String newPassword = generateRandomPassword();
        user.setPassword(this.passwordEncoder.encode(newPassword));
        this.userRepositories.save(user);
        Map<String, Object> data = new HashMap<>();
        data.put("password", newPassword);

        return SuccessResponse.builder().addMessage("Cập nhật thông tin thành công")
                .setData(data).returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updateUserStatus(Integer id) {
        User user = this.userRepositories.getUserById(id);
        if (user == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy người dùng với id = "+id);
        if (user.isStatus())
            user.setStatus(false);
        else
            user.setStatus(true);
        this.userRepositories.save(user);
        return SuccessResponse.builder().addMessage("Cập nhật trạng thái thành công")
                .setData(new UserResponse(user)).returnUpdated();
    }

    private String generateRandomPassword() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        String pwd = RandomStringUtils.random( 15, characters );
        return pwd;
    }
}
