package com.hcmute.fneststorebe.service;

import com.hcmute.fneststorebe.domain.base.ResponseBaseAbstract;
import com.hcmute.fneststorebe.domain.base.SuccessResponse;
import com.hcmute.fneststorebe.domain.dto.request.CreateProductReviewRequest;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.dto.response.ProductReviewResponse;
import com.hcmute.fneststorebe.domain.entities.*;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.repository.OrderItemRepository;
import com.hcmute.fneststorebe.repository.OrderRepository;
import com.hcmute.fneststorebe.repository.ProductRepository;
import com.hcmute.fneststorebe.repository.ProductReviewRepository;
import com.hcmute.fneststorebe.service.interfaces.ProductReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {
    @Autowired
    private ProductReviewRepository productReviewRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public ResponseBaseAbstract createReview(User user, CreateProductReviewRequest request) {
        ProductReview productReview = new ProductReview();
        productReview.setUser(user);
        productReview.setPoint(request.getPoint());
        productReview.setContent(request.getContent());
        OrderItem orderItem = this.orderItemRepository.getOrderItemById(request.getOrderItemId());
        if (orderItem == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy order item với id = " +request.getOrderItemId());
        Order order = orderItem.getOrder();
        if (!order.getStatus().equals(OrderStatus.COMPLETED))
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Đơn hàng không thể đánh giá do trạng thái");
        Product product = orderItem.getProduct();
        product.setTotalReviewPoint(product.getTotalReviewPoint() + request.getPoint());
        product.setReviewAmmount(product.getReviewAmmount() + 1);

        this.productRepository.save(product);
        productReview.setProduct(product);
        this.productReviewRepository.save(productReview);
        order.setStatus(OrderStatus.REVIEWED);
        this.orderRepository.save(order);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(new ProductReviewResponse(productReview))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllReviewByProduct(String productName, Integer currentPage, Integer pageSize) {
        Product product = this.productRepository.getProductByName(productName);
        if (product == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với tên là " + productName);
        Pageable pageable = PageRequest.of(currentPage, pageSize, Sort.by("point").descending());
        Page<ProductReview> productReviewPage = this.productReviewRepository.getProductReviewByProductName(productName, pageable);
        List<ProductReview> productReviewList = productReviewPage.getContent();
        List<ProductReviewResponse> responseList = productReviewList.stream().map(productReview -> new ProductReviewResponse(productReview)).toList();
        Integer totalPages = productReviewPage.getTotalPages();
        PagingAndSortingResponse<ProductReviewResponse> data = new PagingAndSortingResponse<>(
                responseList, currentPage, pageSize, totalPages
        );
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

}
