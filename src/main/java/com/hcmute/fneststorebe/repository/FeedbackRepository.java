package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
    @Query("SELECT f FROM Feedback f WHERE f.id=:id")
    Feedback getFeedbackById(@Param("id") Integer id);
}
