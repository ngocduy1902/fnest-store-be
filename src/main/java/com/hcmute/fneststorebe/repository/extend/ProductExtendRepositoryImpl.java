package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.Product;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ProductExtendRepositoryImpl extends ExtendEntityRepositoryBase<Product> implements ProductExtendRepository{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<Product> searchProduct(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> productRoot = criteriaQuery.from(Product.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                productRoot,
                Product.class.getDeclaredFields()
        );
    }
}
