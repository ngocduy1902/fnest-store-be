package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class UserExtendRepositoryImpl extends ExtendEntityRepositoryBase<User> implements UserExtendRepository {
    private final String INVALID_PARAMETER_MESSAGE = "Tham số không hợp lệ";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<User> searchUser(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                userRoot,
                User.class.getDeclaredFields()
        );
    }
}
