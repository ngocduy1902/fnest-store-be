package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.Post;
import com.hcmute.fneststorebe.domain.entities.Product;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class PostExtendImpl extends ExtendEntityRepositoryBase<Post> implements PostExtend{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<Post> searchPost(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Post> criteriaQuery = criteriaBuilder.createQuery(Post.class);
        Root<Post> postRoot = criteriaQuery.from(Post.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                postRoot,
                Post.class.getDeclaredFields()
        );
    }
}
