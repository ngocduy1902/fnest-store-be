package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.User;

import java.util.Map;

public interface UserExtendRepository {
    PagingAndSortingResponse<User> searchUser(Map<String, String> queries);
}
