package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.Post;

import java.util.Map;

public interface PostExtend {
    PagingAndSortingResponse<Post> searchPost(Map<String, String> queries);
}
