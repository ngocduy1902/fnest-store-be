package com.hcmute.fneststorebe.repository.extend;

import com.hcmute.fneststorebe.domain.dto.response.PagingAndSortingResponse;
import com.hcmute.fneststorebe.domain.entities.Product;

import java.util.Map;

public interface ProductExtendRepository {
    PagingAndSortingResponse<Product> searchProduct(Map<String, String> queries);
}
