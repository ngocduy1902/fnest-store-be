package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {
    @Query("SELECT o FROM OrderItem o WHERE o.id=:id AND o.deletedAt is null")
    OrderItem getOrderItemById(@Param("id") Integer id);

    @Query("SELECT o FROM OrderItem o WHERE o.product.id=:id AND o.deletedAt is null")
    OrderItem getOrderItemByProductId(@Param("id") Integer id);
}
