package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.DeliveryAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress, Integer> {
    @Query("SELECT d FROM DeliveryAddress d WHERE d.user.id=:id")
    List<DeliveryAddress> getAllDeliveryAddressByUserId(@Param("id") Integer id);

    @Query("SELECT d FROM DeliveryAddress d WHERE d.id=:id AND d.deletedAt is null")
    DeliveryAddress getDeliveryAddressById(@Param("id") Integer id);
}
