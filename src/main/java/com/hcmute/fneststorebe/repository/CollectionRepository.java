package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CollectionRepository extends JpaRepository<Collection, Integer> {
    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN TRUE ELSE FALSE END FROM Collection c WHERE c.id=:id AND c.deletedAt is null")
    boolean existsById(@Param("id") Integer id);

    @Query("SELECT c FROM Collection c WHERE c.id=:id AND c.deletedAt is null")
    Collection getCollectionById(@Param("id") Integer id);
}
