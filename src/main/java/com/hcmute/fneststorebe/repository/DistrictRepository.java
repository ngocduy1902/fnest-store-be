package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    @Query("SELECT d FROM District d WHERE d.provinceCity.name=:name")
    List<District> getAllDistrictsByProvinceCityName(@Param("name") String name);
}
