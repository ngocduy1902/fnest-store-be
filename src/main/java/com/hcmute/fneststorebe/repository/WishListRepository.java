package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.WishList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishListRepository extends JpaRepository<WishList, Integer> {
}
