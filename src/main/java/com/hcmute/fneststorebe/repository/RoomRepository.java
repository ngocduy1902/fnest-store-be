package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoomRepository extends JpaRepository<Room, Integer> {
    @Query("SELECT r FROM Room r WHERE r.id=:id AND r.deletedAt is null")
    Room getRoomById(@Param("id") Integer id);

    @Query("SELECT r FROM Room r WHERE r.name=:name AND r.deletedAt is null")
    Room getRoomByName(@Param("name") String name);
}
