package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.ProvinceCity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinceCityRepository extends JpaRepository<ProvinceCity, Integer> {
}
