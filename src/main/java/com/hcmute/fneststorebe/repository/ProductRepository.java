package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Product;
import com.hcmute.fneststorebe.repository.extend.ProductExtendRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer>, ProductExtendRepository {
    @Query("SELECT p FROM Product p WHERE p.category.name=:name AND p.deletedAt is null")
    Page<Product> getProductByCategoryName(@Param("name") String name, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.collection.id=:id AND p.deletedAt is null")
    Page<Product> getProductByCollectionId(@Param("id") Integer id, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.id=:id AND p.deletedAt is null")
    Product getProductById(@Param("id") Integer id);

    @Query("SELECT p FROM Product p WHERE p.isFeatured = true AND p.deletedAt is null")
    Page<Product> getFeaturedProductPage(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.name=:name AND p.deletedAt is null")
    Product getProductByName(@Param("name") String name);

    @Query("SELECT p FROM Product p WHERE p.sold>0 AND p.deletedAt is null")
    List<Product> getSoldProduct();
}
