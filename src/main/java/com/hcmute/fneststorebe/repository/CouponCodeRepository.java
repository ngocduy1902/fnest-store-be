package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.CouponCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CouponCodeRepository extends JpaRepository<CouponCode, Integer> {
    @Query("SELECT c FROM CouponCode c WHERE c.code=:code AND c.deletedAt is null")
    CouponCode getCouponCodeByCode(@Param("code") String code);
    @Query("SELECT c FROM CouponCode c WHERE c.id=:id AND c.deletedAt is null")
    CouponCode getCouponCodeById(@Param("id") Integer id);
}
