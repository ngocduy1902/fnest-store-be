package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN TRUE ELSE FALSE END FROM Category c WHERE c.name=:name AND c.deletedAt is null")
    boolean existByName(@Param("name") String name);

    @Query("SELECT c FROM Category c WHERE c.id=:id AND c.deletedAt is null ")
    Category getCategoryById(@Param("id") Integer id);

    @Query("SELECT c FROM Category c WHERE c.room.id=:id AND c.deletedAt is null")
    List<Category> getCategoryByRoomId(@Param("id") Integer id);
}
