package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Post;
import com.hcmute.fneststorebe.repository.extend.PostExtend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer>, PostExtend {
    @Query("SELECT p FROM Post p WHERE p.id=:id")
    Post getPostById(@Param("id") Integer id);

    @Query("SELECT p FROM Post p")
    Page<Post> getPostPage(Pageable pageable);

    @Query("SELECT p FROM Post p WHERE :name LIKE CONCAT('%', p.banners, '%')")
    List<Post> findPostByBanner(@Param("name") String name);
}
