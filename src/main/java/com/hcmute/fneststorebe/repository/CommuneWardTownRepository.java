package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.CommuneWardTown;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommuneWardTownRepository extends JpaRepository<CommuneWardTown, Integer> {
    @Query("SELECT c FROM CommuneWardTown c WHERE c.district.name=:name")
    List<CommuneWardTown> getAllCommuneWardTownByDistrictName(@Param("name") String name);

    @Query("SELECT c FROM CommuneWardTown c WHERE c.name=:name")
    CommuneWardTown getCommuneWardTownByName(@Param("name") String name);
}
