package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.ProductReview;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductReviewRepository extends JpaRepository<ProductReview, Integer> {
    @Query("SELECT p FROM ProductReview p WHERE p.id=:id AND p.deletedAt is null")
    ProductReview getProductReviewById(@Param("id") Integer id);

    @Query("SELECT p FROM ProductReview p WHERE p.user.id=:id AND p.deletedAt is null")
    List<ProductReview> getProductReviewByUserId(@Param("id") Integer id);

    @Query("SELECT p FROM ProductReview p WHERE p.product.name=:name AND p.deletedAt is null")
    Page<ProductReview> getProductReviewByProductName(@Param("name") String name, Pageable pageable);
}
