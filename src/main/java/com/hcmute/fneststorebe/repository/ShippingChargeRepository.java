package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.ShippingCharge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShippingChargeRepository extends JpaRepository<ShippingCharge, Integer> {
    @Query("SELECT s FROM ShippingCharge s WHERE s.charge=:charge")
    ShippingCharge getShippingCharge(@Param("charge") Double charge);
}
