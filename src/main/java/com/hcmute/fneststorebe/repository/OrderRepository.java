package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.Order;
import com.hcmute.fneststorebe.domain.enums.OrderStatus;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    @Query("SELECT o FROM Order o WHERE o.user.id=:id AND o.deletedAt is null ")
    Page<Order> getOrderByUserId(@Param("id") Integer id, Pageable pageable);

    @Query("SELECT o FROM Order o WHERE o.couponCode.id=:id AND o.deletedAt is null")
    List<Order> getOrderByCouponCode(@Param("id") Integer id);

    @Query("SELECT o FROM Order o WHERE o.id=:id AND o.deletedAt is null")
    Order getOrderById(@Param("id") Integer id);

    @Query("SELECT o FROM Order o WHERE o.status=:status AND o.deletedAt is null")
    Page<Order> getOrderByStatus(@Param("status") OrderStatus status, Pageable pageable);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate)")
    Integer getNumOfOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate)")
    Integer getNumOfOrderByDay(@Param("givenDate")Date givenDate);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate)")
    List<Order> getOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate)")
    List<Order> getOrderByDay(@Param("givenDate")Date givenDate);
}
