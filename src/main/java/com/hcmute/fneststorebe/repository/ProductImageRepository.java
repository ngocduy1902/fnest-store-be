package com.hcmute.fneststorebe.repository;

import com.hcmute.fneststorebe.domain.entities.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductImageRepository extends JpaRepository<ProductImage, Integer> {
    @Query("SELECT i FROM ProductImage i WHERE i.product.id=:id AND i.deletedAt is null")
    List<ProductImage> getProductImageByProductId(@Param("id") Integer id);

    @Query("SELECT i FROM ProductImage i WHERE i.id=:id AND i.deletedAt is null")
    ProductImage getProductImageById(@Param("id") Integer id);
}
