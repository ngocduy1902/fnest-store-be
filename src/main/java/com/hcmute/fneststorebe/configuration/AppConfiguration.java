package com.hcmute.fneststorebe.configuration;

import com.hcmute.fneststorebe.domain.cache.OtpCache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
    @Value("${com.hcmute.fneststorebe.security.otp.expired-after-mins}")
    private long expiredAfterMins;

    @Value("${com.hcmute.fneststorebe.security.otp.code-length}")
    private int otpLength;

    @Bean
    OtpCache otpCache() {
        return new OtpCache(expiredAfterMins, otpLength);
    }
}
