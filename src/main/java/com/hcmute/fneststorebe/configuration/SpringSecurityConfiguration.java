package com.hcmute.fneststorebe.configuration;


import com.hcmute.fneststorebe.configuration.security.CustomAccessDeniedHandler;
import com.hcmute.fneststorebe.configuration.security.CustomAuthenticationEntryPoint;
import com.hcmute.fneststorebe.domain.entities.User;
import com.hcmute.fneststorebe.domain.enums.UserRole;
import com.hcmute.fneststorebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.fneststorebe.jwt.JwtAuthenticationFilter;
import com.hcmute.fneststorebe.oauth.CustomOAuthUserService;
import com.hcmute.fneststorebe.oauth.OAuthAuthenticationSuccessHandler;
import com.hcmute.fneststorebe.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration{

    @Autowired
    private CustomOAuthUserService oAuth2UserService;

    @Autowired
    private OAuthAuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;

    @Autowired
    @Lazy
    private UserRepository userRepositories;

    @Autowired
    @Lazy
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        MvcRequestMatcher.Builder mvcMatcherBuilder = new MvcRequestMatcher.Builder(introspector);
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeHttpRequests(rq -> rq
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/**")).hasAuthority(UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/buyer/**")).hasAuthority(UserRole.BUYER.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("api/staff/**")).hasAnyAuthority(UserRole.ADMIN.name(),UserRole.STAFF.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("api/user/**")).hasAnyAuthority(UserRole.ADMIN.name(),UserRole.STAFF.name(),UserRole.BUYER.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll())
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .oauth2Login()
                .userInfoEndpoint().userService(oAuth2UserService)
                .and()
                .successHandler(oAuth2AuthenticationSuccessHandler)
                .and()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling(e -> {
                    e.authenticationEntryPoint(customAuthenticationEntryPoint);
                    e.accessDeniedHandler(customAccessDeniedHandler);
                });
        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                User userEntity = userRepositories.getUserByUsername(username);
                if (userEntity == null) {
                    throw new UsernameNotFoundException("Không tìm thấy người dùng với username: " + username);
                }
                return userEntity;
            }
        };
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
